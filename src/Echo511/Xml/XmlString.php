<?php

namespace Echo511\Xml;

use Fwk\Xml\Exceptions\XmlError;
use Fwk\Xml\XmlFile;

class XmlString extends XmlFile
{

	private $string;

	public function __construct($string)
	{
		parent::__construct(__FILE__);
		$this->string = $string;
	}



	public function open()
	{
		if (!isset($this->xml)) {
			if (!$this->xml = simplexml_load_string($this->string)) {
				$error = sprintf(
					"%s [%s]", libxml_get_last_error()->message, \libxml_get_last_error()->code
				);

				throw new XmlError($error);
			}
		}

		return $this->xml;
	}



}

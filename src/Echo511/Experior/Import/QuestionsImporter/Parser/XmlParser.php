<?php

namespace Echo511\Experior\Import\QuestionsImporter\Parser;

use Echo511\Experior\Domain\Question;
use Echo511\Experior\Domain\User;
use Echo511\Experior\Import\QuestionsImporter\IParser;
use Echo511\Xml\XmlString;
use Fwk\Xml\Map;
use Fwk\Xml\Path;
use Nette\Object;

/**
 * Parse JSON string, return questions with answers.
 * 
 * Sample stored in assets.
 * 
 * @author Nikolas Tsiongas
 */
class XmlParser extends Object implements IParser
{

	/** @var ArrayParser */
	private $arrayParser;

	function __construct(ArrayParser $arrayParser)
	{
		$this->arrayParser = $arrayParser;
	}



	/**
	 * @param string $string
	 * @param User $user
	 * @return Question[]
	 */
	public function parse($string)
	{
		$map = (new Map())
			->add(Path::factory('/questions/question', 'questions')
			->loop(true)
			->attribute('text')
			->addChildren($answer = Path::factory('answers/answer', 'answers')
				->loop(true)
				->attribute('isCorrect')
				->filter(function($value) {
					if ($value == 'true') {
						return true;
					} elseif ($value == 'false') {
						return false;
					} else {
						return $value;
					}
				})
				->value('text')
			)
			->addChildren($answer = Path::factory('tags/tag', 'tags')
				->loop(true)
				->value('title')
			)
		);

		$xml = new XmlString($string);
		$array = $map->execute($xml);

		return $this->arrayParser->parse($array['questions']);
	}



}

<?php

namespace Echo511\Experior\Import\QuestionsImporter\Parser;

use Echo511\Experior\Domain\Question;
use Echo511\Experior\Domain\User;
use Echo511\Experior\Import\QuestionsImporter\IParser;
use Exception;
use Nette\Object;
use Nette\Utils\Strings;

/**
 * Parse user friendly input, return questions with answers.
 * 
 * @todo Not in working state yet.
 * @author Nikolas Tsiongas
 */
class TextParser extends Object implements IParser
{

	/** @var ArrayParser */
	private $arrayParser;

	function __construct(ArrayParser $arrayParser)
	{
		$this->arrayParser = $arrayParser;
	}



	/**
	 * @param string $string
	 * @param User $user
	 * @return Question[]
	 */
	public function parse($string)
	{
		throw new Exception("Text parsing is not supported!");
		$questions = [];

		$string = implode("\n", array_map('trim', explode("\n", $string)));

		foreach (preg_split("#\n\s*\n#Uis", $string) as $paragraph) {
			// more than one space is possible between questions
			$paragraph = trim($paragraph);
			$parts = explode("\n", $paragraph);
			// first line of block is the question itself
			$questionText = trim($parts[0]);
			unset($parts[0]);

			$answers = [];
			foreach ($parts as $line) {
				$isCorrect = Strings::startsWith($line, '+') ? true : false;
				$text = ltrim($line, '+-');
				$text = trim($text);
				$answers[] = [
				    'text' => $text,
				    'isCorrect' => $isCorrect
				];
			}

			$questions[] = [
			    'text' => $questionText,
			    'answers' => $answers
			];
		}

		return $this->arrayParser->parse($questions);
	}



}

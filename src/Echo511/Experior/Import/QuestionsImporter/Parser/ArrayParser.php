<?php

namespace Echo511\Experior\Import\QuestionsImporter\Parser;

use Doctrine\Common\Collections\ArrayCollection;
use Echo511\Experior\Domain\Question;
use Echo511\Experior\Domain\User;
use Echo511\Experior\Import\QuestionsImporter\IParser;
use Echo511\Experior\Import\QuestionsImporter\ParserException;
use Echo511\Experior\Manipulator\QuestionManipulator;
use Exception;
use Nette\Object;
use Tracy\Debugger;

/**
 * Parse array, return questions.
 * 
 * [
 * 	[
 * 		'text' => 'What is this?',
 * 		'answers' => [
 * 			[
 * 				'text' => 'A giraffe',
 * 				'isCorrect' => false
 * 			]
 * 		],
 * 		'tags' => ['Zoo', 'Animals']
 * 	]
 * ]
 * 
 * @author Nikolas Tsiongas
 */
class ArrayParser extends Object implements IParser
{

	/** @var QuestionManipulator */
	private $questionManipulator;

	function __construct(QuestionManipulator $questionManipulator)
	{
		$this->questionManipulator = $questionManipulator;
	}



	/**
	 * @param string $string
	 * @param User $user
	 * @return Question[]
	 * @throws ParserException
	 */
	public function parse($array)
	{
		try {
			$questions = new ArrayCollection;
			foreach ($array as $questionItem) {
				$questions[] = $question = $this->questionManipulator->createQuestion()
					->setText($questionItem['text']);

				foreach ($questionItem['answers'] as $answerItem) {
					$this->questionManipulator->addAnswer($question, $answerItem['text'], $answerItem['isCorrect']);
				}

				if (isset($questionItem['tags'])) {
					foreach ($questionItem['tags'] as $tagTitle) {
						$this->questionManipulator->addTag($question, $tagTitle);
					}
				}
			}
			return $questions;
		} catch (Exception $e) {
			Debugger::log($e);
			throw new ParserException;
		}
	}



}

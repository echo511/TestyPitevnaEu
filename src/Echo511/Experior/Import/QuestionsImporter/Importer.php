<?php

namespace Echo511\Experior\Import\QuestionsImporter;

use Doctrine\Common\Collections\Collection;
use Echo511\Experior\Domain\QuestionsImport;
use Echo511\Experior\Domain\User;
use Kdyby\Doctrine\EntityManager;
use Nette\Object;

/**
 * Import questions from JSON string into system.
 * 
 * @author Nikolas Tsiongas
 */
class Importer extends Object
{

	/** @var EntityManager */
	private $em;

	public function __construct(EntityManager $em)
	{
		$this->em = $em;
	}



	/**
	 * @param string $string
	 * @param User $user
	 */
	public function import(Collection $questions, User $user)
	{
		foreach ($questions as $question) {
			$question->setCreatedBy($user);
		}
		$import = (new QuestionsImport)
			->setCreatedBy($user)
			->setQuestions($questions);
		$this->em->persist($import);
		$this->em->flush();
		return $import;
	}



}

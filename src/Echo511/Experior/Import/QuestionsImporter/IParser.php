<?php

namespace Echo511\Experior\Import\QuestionsImporter;

use Echo511\Experior\Domain\User;

/**
 * Parse string, return questions with answers.
 * 
 * @author Nikolas Tsiongas
 */
interface IParser
{

	/**
	 * @param string $string
	 * @param User $user
	 */
	function parse($string);
}

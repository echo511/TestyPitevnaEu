<?php

namespace Echo511\Experior\Command;

use Echo511\Experior\Domain\Question;
use Echo511\Experior\Export\XmlExporter;
use Kdyby\Doctrine\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Cli command that exports last versions of all questions.
 * 
 * @author Nikolas Tsiongas
 */
class ExportAllQuestionsCommand extends Command
{

	/** @var EntityManager */
	private $em;

	/** @var XmlExporter */
	private $xmlExporter;

	function __construct(EntityManager $em, XmlExporter $xmlExporter)
	{
		parent::__construct();
		$this->em = $em;
		$this->xmlExporter = $xmlExporter;
	}



	public function configure()
	{
		$this->setName('experior:export:questions')
			->setDescription('Export all questions to a given format. XML by default.');
		$this->addOption('format', 'f', InputOption::VALUE_OPTIONAL, 'Export format.', 'xml');
	}



	public function execute(InputInterface $input, OutputInterface $output)
	{
		$format = $input->getOption('format');
		if ($format == 'xml') {
			$output->write($this->xmlExporter->export($this->em->getDao(Question::classname)->findBy(['lastVersion' => null])));
		} else {
			$output->write('Unsupported format.');
		}
	}



}

<?php

namespace Echo511\Experior\Export;

use Echo511\Experior\Domain\Question;
use Nette\Object;

/**
 * @author Nikolas Tsiongas
 */
class XmlExporter extends Object
{

	/**
	 * @param Question[] $questions
	 * @return string
	 */
	public function export($questions)
	{
		$xml = "<questions>\n\n";
		foreach ($questions as $question) {
			$xml .= "\t<question text=\"" . $question->getText() . "\">\n";
			$xml .= "\t\t<answers>\n";
			foreach ($question->getAnswers() as $answer) {
				$xml .= "\t\t\t<answer isCorrect=\"" . ($answer->getIsCorrect() ? "true" : "false") . "\">";
				$xml .= $answer->getText();
				$xml .= "</answer>\n";
			}
			$xml .= "\t\t</answers>\n";
			if ($question->getTags()->count() > 0) {
				$xml .= "\t\t<tags>\n";
				foreach ($question->getTags() as $tag) {
					$xml .= "\t\t\t<tag>";
					$xml .= $tag->getTitle();
					$xml .= "</tag>\n";
				}
				$xml .= "\t\t</tags>\n";
			}
			$xml .= "\t</question>\n\n";
		}
		$xml .= "</questions>";
		return $xml;
	}



}

<?php

namespace Echo511\Experior\Control;

/**
 * @author Nikolas Tsiongas
 */
interface IEditableQuestionControlFactory
{

	/**
	 * @return EditableQuestionControl
	 */
	function create($question);
}

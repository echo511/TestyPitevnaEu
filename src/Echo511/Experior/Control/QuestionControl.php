<?php

namespace Echo511\Experior\Control;

use Echo511\Experior\Domain\Answer;
use Echo511\Experior\Domain\Question;
use Echo511\Experior\Template\TemplateLocator;
use Latte\Template;
use Nette\Application\UI\Control;

/**
 * @author Nikolas Tsiongas
 */
class QuestionControl extends Control
{

	const VERSION_FIRST = 'first';
	const VERSION_LAST = 'last';

	/** @var string Specifies what version is rendered */
	protected $show;

	/** @var Question */
	private $question;

	/** @var TemplateLocator */
	private $templateLocator;

	public function __construct(Question $question, TemplateLocator $templateLocator)
	{
		$this->question = $question;
		$this->templateLocator = $templateLocator;
		$this->show = self::VERSION_LAST;
	}



	/**
	 * Show first version of the question.
	 */
	public function handleShowFirst()
	{
		$this->show = self::VERSION_FIRST;
		$this->redrawControl();
	}



	/**
	 * Show last version of the question.
	 */
	public function handleShowLast()
	{
		$this->show = self::VERSION_LAST;
		$this->redrawControl();
	}



	public function render()
	{
		$this->createTemplate()->render();
	}



	/**
	 * @return Template
	 */
	protected function createTemplate()
	{
		$template = parent::createTemplate();
		$template->setFile($this->templateLocator->locate('Question', 'default'));
		$template->questionBlocksTemplateFile = $this->templateLocator->locate('Question', 'blocks');
		$template->question = $this->question;
		$template->show = $this->show;
		$template->encodeAnswer = function(Answer $answer) {
			return $this->encodeAnswer($answer);
		};
		return $template;
	}



	/**
	 * Encodes answer's composite key into base64 encoded string.
	 * 
	 * @param Answer $answer
	 * @return string
	 */
	protected function encodeAnswer(Answer $answer)
	{
		return base64_encode(json_encode([
		    'text' => $answer->getText(),
		    'isCorrect' => $answer->getIsCorrect()
		]));
	}



	/**
	 * Decodes answer's composite key as associative array from base64 encoded string.
	 * 
	 * @param string $string
	 * @return array
	 */
	protected function decodeAnswer($string)
	{
		return (array) json_decode(base64_decode($string));
	}



}

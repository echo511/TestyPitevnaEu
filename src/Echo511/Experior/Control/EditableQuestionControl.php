<?php

namespace Echo511\Experior\Control;

use Echo511\Experior\Domain\Question;
use Echo511\Experior\Manipulator\QuestionManipulator;
use Echo511\Experior\Template\TemplateLocator;
use Kdyby\Doctrine\EntityManager;
use Latte\Template;
use Nette\Utils\Strings;

/**
 * Edit question inline with X-Editable.
 * 
 * @author Nikolas Tsiongas
 */
class EditableQuestionControl extends QuestionControl
{

	const MODE_EDITABLE = true;
	const MODE_SIMPLE = false;
	const DELETE_TAG = '';

	/** @var Question */
	private $question;

	/** @var EntityManager */
	private $em;

	/** @var QuestionManipulator */
	private $questionManipulator;

	/** @var TemplateLocator */
	private $templateLocator;

	/** @var boolean */
	private $isEditable;

	public function __construct(Question $question, EntityManager $em, QuestionManipulator $questionManipulator, TemplateLocator $templateLocator)
	{
		parent::__construct($question, $templateLocator);
		$this->question = $question;
		$this->em = $em;
		$this->questionManipulator = $questionManipulator;
		$this->templateLocator = $templateLocator;
		$this->isEditable = self::MODE_SIMPLE;
	}



	/**
	 * @return Question
	 */
	public function getQuestion()
	{
		return $this->question;
	}



	/**
	 * Render editable component. Only last version is editable.
	 */
	public function handleShowEditable()
	{
		$this->isEditable = self::MODE_EDITABLE;
		$this->redrawControl('question');
	}



	/**
	 * Render non editable. QuestionControl decides what version is rendered.
	 */
	public function handleShowDefault()
	{
		$this->isEditable = self::MODE_SIMPLE;
		$this->redrawControl('question');
	}



	/**
	 * Change question text.
	 */
	public function handleChangeQuestion()
	{
		$text = $this->getXEditableValue();

		$this->questionManipulator->changeText($this->addQuestionVersion($this->question), $text);

		$this->em->persist($this->question);
		$this->em->flush();

		$this->redrawControl('question');
	}



	/**
	 * Add new answer to question.
	 */
	public function handleNewAnswer()
	{
		$text = $this->getXEditableValue();
		$isCorrect = Strings::startsWith($text, '+') ? true : false;
		$text = ltrim($text, '+-');

		$this->questionManipulator->addAnswer($this->addQuestionVersion($this->question), $text, $isCorrect);

		$this->em->persist($this->question);
		$this->em->flush();

		$this->redrawControl('question');
	}



	/**
	 * Change answer or delete it when empty string provided.
	 */
	public function handleChangeAnswer()
	{
		$previous = $this->getXEditableDecodedAnswer();
		$previousText = $previous['text'];
		$previousIsCorrect = $previous['isCorrect'];
		$text = $this->getXEditableValue();
		$isCorrect = $previousIsCorrect;

		if ($text == self::DELETE_TAG) {
			$this->questionManipulator->removeAnswer($this->addQuestionVersion($this->question), $previousText, $previousIsCorrect);
		} else {
			if (Strings::startsWith($text, '+')) {
				$text = ltrim($text, '+-');
				$isCorrect = true;
			} elseif (Strings::startsWith($text, '-')) {
				$text = ltrim($text, '+-');
				$isCorrect = false;
			}
			$this->questionManipulator->changeAnswer($this->addQuestionVersion($this->question), $previousText, $previousIsCorrect, $text, $isCorrect);
		}

		$this->em->persist($this->question);
		$this->em->flush();

		$this->redrawControl('question');
	}



	/**
	 * Assign tag to this question.
	 */
	public function handleNewTag()
	{
		$tagTitle = $this->getXEditableValue();
		$this->questionManipulator->addTag($this->addQuestionVersion($this->question), $tagTitle);
		$this->em->persist($this->question);
		$this->em->flush();

		$this->redrawControl('question');
	}



	/**
	 * Remove previous tag and assign new one. The old tag is not being renamed.
	 */
	public function handleChangeTag()
	{
		$question = $this->addQuestionVersion($this->question);
		$previousTitle = $this->getXEditablePk();
		$title = $this->getXEditableValue();

		if ($title == self::DELETE_TAG) {
			$this->questionManipulator->removeTag($question, $previousTitle);
		} else {
			$this->questionManipulator->changeTag($question, $previousTitle, $title);
		}

		$this->em->persist($question);
		$this->em->flush();

		$this->redrawControl('question');
	}



	public function render()
	{
		$this->createTemplate()->render();
	}



	/**
	 * @return Template
	 */
	protected function createTemplate()
	{
		$template = parent::createTemplate();
		$template->setFile($this->templateLocator->locate('EditableQuestion', 'default'));
		$template->editableQuestionBlocksTemplateFile = $this->templateLocator->locate('EditableQuestion', 'blocks');
		$template->question = $this->question;
		$template->isEditable = $this->isEditable;
		return $template;
	}



	/**
	 * Get x-editable value from post request.
	 * 
	 * @return string
	 */
	protected function getXEditableValue()
	{
		return $this->presenter->request->getPost()['value'];
	}



	/**
	 * Get x-editable pk (key or previous value)
	 * 
	 * @return string
	 */
	protected function getXEditablePk()
	{
		return $this->presenter->request->getPost()['pk'];
	}



	/**
	 * Get answer's composite key.
	 * 
	 * @return array
	 */
	protected function getXEditableDecodedAnswer()
	{
		return $this->decodeAnswer($this->getXEditablePk());
	}



	/**
	 * Add new question version and set author.
	 * 
	 * @param Question $question
	 * @return Question
	 */
	protected function addQuestionVersion(Question $question)
	{
		return $question->addVersion()
				->setCreatedBy($this->presenter->user->getIdentity());
	}



}

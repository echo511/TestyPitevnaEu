<?php

namespace Echo511\Experior\Control;

use Echo511\Experior\Domain\TestQuestion;
use Echo511\Experior\Manipulator\QuestionManipulator;
use Echo511\Experior\Template\TemplateLocator;
use Kdyby\Doctrine\EntityManager;
use Latte\Template;

/**
 * Show mistakes made in answers, allow to edit the question.
 * 
 * @author Nikolas Tsiongas
 */
class EditableTestQuestionControl extends EditableQuestionControl
{

	const VERSION_TEST = 'test';

	/** @var TestQuestion */
	private $testQuestion;

	/** @var TemplateLocator */
	private $templateLocator;

	public function __construct(TestQuestion $testQuestion, EntityManager $em, QuestionManipulator $questionManipulator, TemplateLocator $templateLocator)
	{
		parent::__construct($testQuestion->getQuestion(), $em, $questionManipulator, $templateLocator);
		$this->testQuestion = $testQuestion;
		$this->templateLocator = $templateLocator;
		$this->show = self::VERSION_TEST;
	}



	/**
	 * Show version of question that was used during the test.
	 */
	public function handleShowTest()
	{
		$this->show = self::VERSION_TEST;
		$this->redrawControl();
	}



	/**
	 * @return Template
	 */
	protected function createTemplate()
	{
		$template = parent::createTemplate();
		$template->setFile($this->templateLocator->locate('EditableTestQuestion', 'default'));
		$template->editableTestQuestionBlocksTemplateFile = $this->templateLocator->locate('EditableTestQuestion', 'blocks');
		$template->testQuestion = $this->testQuestion;
		return $template;
	}



}

<?php

namespace Echo511\Experior\Control;

/**
 * @author Nikolas Tsiongas
 */
interface IEditableTestQuestionControlFactory
{

	/**
	 * @return EditableTestQuestionControl
	 */
	function create($testQuestion);
}

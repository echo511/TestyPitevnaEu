<?php

namespace Echo511\Experior\Query;

use Doctrine\ORM\Query;
use Echo511\Experior\Domain\QuestionsImport;
use Echo511\Experior\Domain\QuestionTag;
use Kdyby\Doctrine\QueryObject;
use Kdyby\Persistence\Queryable;

/**
 * Fetch tags of those questions that were imported together.
 * 
 * @author Nikolas Tsiongas
 */
final class QuestionsImportTagQuery extends QueryObject
{

	private $id;

	public function __construct(QuestionsImport $import)
	{
		parent::__construct();
		$this->id = $import->getId();
	}



	/**
	 * @param Queryable $repository
	 * @return Query
	 */
	protected function doCreateQuery(Queryable $repository)
	{
		$qb = $repository->createQueryBuilder();
		return $repository->createQueryBuilder()
				->select('t')
				->from(QuestionTag::classname, 't')
				->where($qb->expr()->in('t.title', $repository->createQueryBuilder()
						->select('t2.title')
						->from(QuestionsImport::classname, 'i')
						->leftJoin('i.questions', 'q')
						->leftJoin('q.lastVersion', 'ql')
						->leftJoin('ql.tags', 't2')
						->where('i.id = :iid')
						->getDQL()
				))
				->orWhere($qb->expr()->in('t.title', $repository->createQueryBuilder()
						->select('t3.title')
						->from(QuestionsImport::classname, 'i2')
						->leftJoin('i2.questions', 'q2')
						->leftJoin('q2.tags', 't3')
						->where('i2.id = :iid')
						->getDQL()
				))
				->setParameter('iid', $this->id)
				->getQuery();
	}



}

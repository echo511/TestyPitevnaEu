<?php

namespace Echo511\Experior\Query;

use Doctrine\ORM\Query;
use Echo511\Experior\Domain\Question;
use Echo511\Experior\Domain\QuestionsImport;
use Kdyby\Doctrine\QueryObject;
use Kdyby\Persistence\Queryable;

/**
 * Fetch questions that were imported together.
 * 
 * @author Nikolas Tsiongas
 */
class QuestionsImportDetailQuery extends QueryObject
{

	/** @var int */
	private $id;

	public function __construct(QuestionsImport $import)
	{
		$this->id = $import->getId();
	}



	/**
	 * @param Queryable $repository
	 * @return Query
	 */
	protected function doCreateQuery(Queryable $repository)
	{
		return $repository->createQueryBuilder()
				->select('q')
				->from(Question::classname, 'q')
				// saturation
				->leftJoin('q.lastVersion', 'qlv')->addSelect('qlv')
				->leftJoin('qlv.answers', 'a')->addSelect('a')
				->leftJoin('qlv.tags', 't')->addSelect('t')
				// limit import
				->leftJoin('q.questionsImport', 'i')
				->where('i = :iid', $this->id)
				->getQuery();
	}



	/**
	 * @param Queryable $repository
	 * @return Query
	 */
	protected function doCreateCountQuery(Queryable $repository)
	{
		return $repository->createQueryBuilder()
				->select('COUNT(q)')
				->from(Question::classname, 'q')
				// limit import
				->leftJoin('q.questionsImport', 'i')
				->where('i = :iid', $this->id)
				->getQuery();
	}



}

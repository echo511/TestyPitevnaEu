<?php

namespace Echo511\Experior\Query;

use Doctrine\ORM\Query;
use Echo511\Experior\Domain\Question;
use Echo511\Experior\Domain\QuestionTag;
use Kdyby\Doctrine\QueryObject;
use Kdyby\Persistence\Queryable;

/**
 * Fetch questions that belong to the specified tag.
 * 
 * @author Nikolas Tsiongas
 */
final class QuestionTagDetailQuery extends QueryObject
{

	/** @var string */
	private $title;

	public function __construct(QuestionTag $tag)
	{
		$this->title = $tag->getTitle();
	}



	/**
	 * @param Queryable $repository
	 * @return Query
	 */
	protected function doCreateQuery(Queryable $repository)
	{
		$expr = new Query\Expr;
		return $repository->createQueryBuilder()
				->select('q')
				->from(Question::classname, 'q')
				// saturation
				->leftJoin('q.answers', 'a')->addSelect('a')
				->leftJoin('q.tags', 't')->addSelect('t')
				// limit last version & tag
				->andWhere($expr->isNull('q.lastVersion'))
				->leftJoin('q.tags', 't2')
				->andWhere('t2.title = :title', $this->title)
				->getQuery();
	}



	/**
	 * @param Queryable $repository
	 * @return Query
	 */
	protected function doCreateCountQuery(Queryable $repository)
	{
		$expr = new Query\Expr;
		return $repository->createQueryBuilder()
				->select('COUNT(q)')
				->from(Question::classname, 'q')
				// limit last version & tag
				->andWhere($expr->isNull('q.lastVersion'))
				->leftJoin('q.tags', 't2')
				->andWhere('t2.title = :title', $this->title)
				->getQuery();
	}



}

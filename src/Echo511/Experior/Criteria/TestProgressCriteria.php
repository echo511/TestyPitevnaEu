<?php

namespace Echo511\Experior\Criteria;

use Doctrine\Common\Collections\Criteria;
use Echo511\Experior\Domain\User;

/**
 * @author Nikolas Tsiongas
 */
class TestProgressCriteria extends Criteria
{

	public function filterCreatedBy(User $user)
	{
		$this->andWhere(self::expr()->eq('createdBy', $user));
		return $this;
	}



	public function filterIncomplete()
	{
		$this->andWhere(self::expr()->eq('isComplete', false));
		return $this;
	}



	public function filterComplete()
	{
		$this->andWhere(self::expr()->eq('isComplete', true));
		return $this;
	}



}

<?php

namespace Echo511\Experior\Criteria;

use Doctrine\Common\Collections\Criteria;

/**
 * @author Nikolas Tsiongas
 */
class QuestionCriteria extends Criteria
{

	public function filterId($id)
	{
		$this->andWhere(self::expr()->eq('id', $id));
		$this->setMaxResults(1);
		return $this;
	}



}

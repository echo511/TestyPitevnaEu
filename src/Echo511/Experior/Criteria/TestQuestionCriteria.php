<?php

namespace Echo511\Experior\Criteria;

use Doctrine\Common\Collections\Criteria;
use Echo511\Experior\Domain\TestProgress;

/**
 * @author Nikolas Tsiongas
 */
class TestQuestionCriteria extends Criteria
{

	public function filterId($id)
	{
		$this->andWhere(self::expr()->eq('id', $id));
		$this->setMaxResults(1);
		return $this;
	}



	public function filterTestProgress(TestProgress $testProgress)
	{
		$this->andWhere(self::expr()->eq('testProgress', $testProgress));
		return $this;
	}



	public function filterUnanswered()
	{
		$this->andWhere(self::expr()->isNull('answeredAt'));
		return $this;
	}



	public function filterAnswered()
	{
		$this->andWhere(self::expr()->neq('answeredAt', null));
		return $this;
	}



}

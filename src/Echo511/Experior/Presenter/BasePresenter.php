<?php

namespace Echo511\Experior\Presenter;

use Echo511\Experior\DI\ExperiorExtension;
use Echo511\Experior\Template\TemplateLocator;
use Echo511\Gaufrette\Assets\AssetMacro;
use IPub\Gravatar\Gravatar;
use Latte\Macros\MacroSet;
use Nette\Application\UI\ITemplateFactory;
use Nette\Application\UI\Presenter;

/**
 * Abstract presenter.
 * Register asset macro.
 * Register ifCurrentModule macro.
 * Check permissions.
 * 
 * @author Nikolas Tsiongas
 */
abstract class BasePresenter extends Presenter
{

	/** @var TemplateLocator @inject */
	public $templateLocator;

	/** @var AssetMacro @inject */
	public $assetMacro;

	/** @var Gravatar @inject */
	public $gravatar;

	/** @var ITemplateFactory @inject */
	public $templateFactory;

	public function createTemplate()
	{
		$template = parent::createTemplate();
		$this->assetMacro->register($template, 'asset', ExperiorExtension::classname);
		$template->_gravatar = $this->gravatar;
		$set = new MacroSet($template->getLatte()->getCompiler());
		$set->addMacro('ifCurrentModule', function($node, $writer) {
			$string = trim($node->args, '\'"');
			return $writer->write('if (\Nette\Utils\Strings::startsWith($presenter->getAction(true), \'' . $string . '\')):');
		}, 'endif;');
		$template->getLatte()->addFilter('loader', $this->gravatar->createTemplateHelpers());
		$template->_theme = $this->templateLocator->getTheme();
		return $template;
	}



	public function startup()
	{
		parent::startup();

		if (!$this->user->isLoggedIn() && $this->getAction(true) != ':Experior:Authentication:Authentication:login') {
			$this->redirect(':Experior:Authentication:Authentication:login');
		} elseif ($this->user->isLoggedIn() && $this->getAction(true) == ':Experior:Authentication:Authentication:login') {
			$this->redirect(':Experior:QuestionsImport:List:default');
		}
	}



	public function formatTemplateFiles()
	{
		return $this->templateLocator->locate($this, $this->getView());
	}



	public function formatLayoutTemplateFiles()
	{
		return $this->templateLocator->locateLayout($this);
	}



}

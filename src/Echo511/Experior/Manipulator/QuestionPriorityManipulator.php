<?php

namespace Echo511\Experior\Manipulator;

use Echo511\Experior\Domain\Question;
use Echo511\Experior\Domain\QuestionPriority;
use Echo511\Experior\Domain\User;
use Kdyby\Doctrine\EntityManager;
use Nette\Object;

/**
 * @author Nikolas Tsiongas
 */
class QuestionPriorityManipulator extends Object
{

	/** @var EntityManager */
	private $em;

	function __construct(EntityManager $em)
	{
		$this->em = $em;
	}



	/**
	 * 
	 * @param Question $question
	 * @param User $user
	 * @return QuestionPriority
	 */
	public function getQuestionPriority(Question $question, User $user)
	{
		$questionPriority = $this->em->getDao(QuestionPriority::classname)
			->find([
		    'firstVersionOfQuestion' => $question->getFirstVersion()->getId(),
		    'user' => $user->getId()
		]);

		if ($questionPriority === null) {
			$questionPriority = (new QuestionPriority)
				->setFirstVersionOfQuestion($question->getFirstVersion())
				->setUser($user);
			$this->em->persist($questionPriority);
		}

		return $questionPriority;
	}



}

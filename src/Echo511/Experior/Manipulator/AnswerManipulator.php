<?php

namespace Echo511\Experior\Manipulator;

use Doctrine\ORM\NoResultException;
use Echo511\Experior\Domain\Answer;
use Kdyby\Doctrine\EntityManager;
use Nette\Object;

/**
 * Get answer from storage or create new instance.
 * 
 * @author Nikolas Tsiongas
 */
class AnswerManipulator extends Object
{

	/** @var EntityManager */
	private $em;

	function __construct(EntityManager $em)
	{
		$this->em = $em;
	}



	/**
	 * Get answer from storage or create new instance.
	 * 
	 * @param string $text
	 * @param boolean $isCorrect
	 * @return Answer
	 * @throws NoResultException
	 */
	public function getAnswer($text, $isCorrect)
	{
		try {
			$result = $this->em->getDao(Answer::classname)
				->find(['text' => $text, 'isCorrect' => $isCorrect]);

			if ($result === null) {
				throw new NoResultException;
			}

			return $result;
		} catch (NoResultException $e) {
			$answer = (new Answer())
				->setText($text)
				->setIsCorrect($isCorrect);
			$this->em->persist($answer);
			return $answer;
		}
	}



}

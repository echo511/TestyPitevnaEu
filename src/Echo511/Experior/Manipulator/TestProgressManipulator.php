<?php

namespace Echo511\Experior\Manipulator;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Echo511\Experior\Domain\TestProgress;
use Echo511\Experior\Domain\TestProgressSource;
use Echo511\Experior\Domain\TestQuestion;
use Nette\Object;

/**
 * Create new test progress instance.
 */
class TestProgressManipulator extends Object
{

	/**
	 * Create test progress from provided questions.
	 * !! This will always use the last versions of the questions !!
	 * 
	 * @return TestProgress
	 */
	public function createTestProgress(\Traversable $questions, TestProgressSource $source = null)
	{
		$testQuestions = new ArrayCollection;
		$questions = iterator_to_array($questions);
		shuffle($questions);
		foreach ($questions as $question) {
			$testQuestions[] = (new TestQuestion)
				->setQuestion($question->getLastVersion());
		}

		$testProgress = (new TestProgress())
			->setTestQuestions($testQuestions);

		if ($source instanceof TestProgressSource) {
			$testProgress->setSourceId($source->getId())
				->setSourceType($source->getType());
		}

		return $testProgress;
	}



}

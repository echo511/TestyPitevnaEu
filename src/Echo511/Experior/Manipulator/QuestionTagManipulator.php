<?php

namespace Echo511\Experior\Manipulator;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\NoResultException;
use Echo511\Experior\Domain\QuestionTag;
use Kdyby\Doctrine\EntityManager;
use Nette\Object;
use Nette\Utils\Strings;

/**
 * Get tag from database or create new instance.
 * 
 * @author Nikolas Tsiongas
 */
class QuestionTagManipulator extends Object
{

	/** @var EntityManager */
	private $em;

	function __construct(EntityManager $em)
	{
		$this->em = $em;
	}



	/**
	 * @param string $title
	 * @return QuestionTag
	 */
	public function getTag($title)
	{
		try {
			$result = $this->em->getDao(QuestionTag::classname)
				->find(['title' => $title]);

			if ($result === null) {
				throw new NoResultException;
			}

			return $result;
		} catch (NoResultException $e) {
			$webalize = Strings::webalize($title);
			$present = $this->em->getDao(QuestionTag::classname)
				->findPairs(['title' => $title], 'webalize');

			$newWebalize = $webalize;
			while (in_array($newWebalize, $present)) {
				$newWebalize = $webalize . '-' . Strings::random(3);
			}

			$questionTag = (new QuestionTag)
				->setTitle($title)
				->setWebalize($newWebalize);

			$this->em->persist($questionTag);
			return $questionTag;
		}
	}



	/**
	 * @param array $tagsTitles
	 * @return ArrayCollection
	 */
	public function getTags($tagsTitles)
	{
		$tags = new ArrayCollection;
		foreach ($tagsTitles as $tagTitle) {
			$tags[] = $this->getTag($tagTitle);
		}
		return $tags;
	}



}

<?php

namespace Echo511\Experior\Manipulator;

use Echo511\Experior\Domain\Question;
use Kdyby\Doctrine\EntityManager;
use Nette\Object;

/**
 * Create question, change text, add answers and tags with regards to the database.
 * 
 * @author Nikolas Tsiongas
 */
class QuestionManipulator extends Object
{

	/** @var EntityManager */
	private $em;

	/** @var AnswerManipulator */
	private $answerManipulator;

	/** @var QuestionTagManipulator */
	private $questionTagManipulator;

	function __construct(EntityManager $em, AnswerManipulator $answerManipulator, QuestionTagManipulator $questionTagManipulator)
	{
		$this->em = $em;
		$this->answerManipulator = $answerManipulator;
		$this->questionTagManipulator = $questionTagManipulator;
	}



	/**
	 * @return Question
	 */
	public function createQuestion()
	{
		return new Question;
	}



	/**
	 * !! Use $question->addVersion() before calling !!
	 * 
	 * @param Question $question
	 * @param string $text
	 * @return QuestionManipulator
	 */
	public function changeText(Question $question, $text)
	{
		$question->setText($text);
		return $this;
	}



	/**
	 * !! Use $question->addVersion() before calling !!
	 * 
	 * @param Question $question
	 * @param string $answerText
	 * @param string $answerIsCorrect
	 * @return QuestionManipulator
	 */
	public function addAnswer(Question $question, $answerText, $answerIsCorrect)
	{
		$question->addAnswer($this->answerManipulator->getAnswer($answerText, $answerIsCorrect));
		return $this;
	}



	/**
	 * !! Use $question->addVersion() before calling !!
	 * 
	 * @param Question $question
	 * @param string $answerText
	 * @param string $answerIsCorrect
	 * @return QuestionManipulator
	 */
	public function removeAnswer(Question $question, $answerText, $answerIsCorrect)
	{
		$question->removeAnswer($this->answerManipulator->getAnswer($answerText, $answerIsCorrect));
		return $this;
	}



	/**
	 * !! Use $question->addVersion() before calling !!
	 * 
	 * @param Question $question
	 * @param string $previousText
	 * @param boolean $previousIsCorrect
	 * @param string $text
	 * @param boolean $isCorrect
	 * @return QuestionManipulator
	 */
	public function changeAnswer(Question $question, $previousText, $previousIsCorrect, $text, $isCorrect)
	{
		$this->removeAnswer($question, $previousText, $previousIsCorrect);
		$this->addAnswer($question, $text, $isCorrect);
		return $this;
	}



	/**
	 * !! Use $question->addVersion() before calling !!
	 * 
	 * @param Question $question
	 * @param array $tagsTitles
	 * @return QuestionManipulator
	 */
	public function addTags(Question $question, $tagsTitles)
	{
		$question->setTags($this->questionTagManipulator->getTags($tagsTitles));
		return $this;
	}



	/**
	 * !! Use $question->addVersion() before calling !!
	 * 
	 * @param Question $question
	 * @param string $tagTitle
	 * @return QuestionManipulator
	 */
	public function addTag(Question $question, $tagTitle)
	{
		$question->addTag($this->questionTagManipulator->getTag($tagTitle));
		return $this;
	}



	/**
	 * !! Use $question->addVersion() before calling !!
	 * 
	 * @param Question $question
	 * @param string $tagTitle
	 * @return QuestionManipulator
	 */
	public function removeTag(Question $question, $tagTitle)
	{
		$question->removeTag($this->questionTagManipulator->getTag($tagTitle));
		return $this;
	}



	/**
	 * !! Use $question->addVersion() before calling !!
	 * 
	 * @param Question $question
	 * @param string $previousTagTitle
	 * @param string $tagTitle
	 * @return QuestionManipulator
	 */
	public function changeTag(Question $question, $previousTagTitle, $tagTitle)
	{
		$this->removeTag($question, $previousTagTitle);
		$this->addTag($question, $tagTitle);
		return $this;
	}



}

(function(window, $, undefined) {
	$.nette.ext('x-editable', {
		init: function() {
			this.apply();
		},
		success: function() {
			this.apply();
		}
	}, {
		selectors: [],
		addSelector: function(selector) {
			this.selectors.push(selector);
		},
		apply: function() {
			this.selectors.forEach(function(selector) {
				$.nette.ext('x-editable').makeEditable(selector);
			});
		},
		makeEditable: function(selector) {
			$(selector).editable({
				success: this.success
			});
		},
		success: function(response, newValue) {
			$.nette.ext('snippets').updateSnippets(response.snippets);
			$.nette.ext('x-editable').apply();
		}
	});
})(window, window.jQuery);

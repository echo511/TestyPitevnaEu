<?php

namespace Echo511\Experior\Template;

use Nette\Application\UI\Presenter;
use Nette\Object;

/**
 * Locates templates for presenters, components. Supports theme packaging.
 * 
 * @author Nikolas Tsiongas
 */
class TemplateLocator extends Object
{

	/**
	 * Theme directory
	 */
	private $themeDir;
	private $theme;

	public function __construct($themeDir)
	{
		$this->themeDir = $themeDir;
		$this->theme = 'default';
	}



	public function setTheme($theme)
	{
		$this->theme = $theme;
		return $this;
	}



	public function getTheme()
	{
		return $this->theme;
	}



	/**
	 * Locate template for control.
	 * 
	 * @param Presenter|string $control
	 * @param string $action
	 */
	public function locate($control, $action)
	{
		if ($control instanceof Presenter) {
			return $this->locatePresenterTemplate($control, $action);
		}

		return $this->locateControlTemplate($control, $action);
	}



	/**
	 * Locate presenter's layout.
	 * 
	 * @param Presenter $presenter
	 */
	public function locateLayout(Presenter $presenter)
	{
		$possible = [];
		$modules = $this->extractPresenterModules($presenter);
		while (!empty($modules)) {
			$possible[] = $this->themeDir . DIRECTORY_SEPARATOR . 'presenters' . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $modules) . DIRECTORY_SEPARATOR . '@layout.latte';
			unset($modules[count($modules) - 1]);
		}
		$possible[] = $this->themeDir . DIRECTORY_SEPARATOR . 'presenters' . DIRECTORY_SEPARATOR . '@layout.latte';
		return $possible;
	}



	protected function locatePresenterTemplate(Presenter $presenter, $action)
	{
		return [$this->themeDir . DIRECTORY_SEPARATOR . 'presenters' . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $this->extractPresenterModules($presenter)) . '.' . $action . '.latte'];
	}



	protected function locateControlTemplate($control, $action)
	{
		return $this->themeDir . DIRECTORY_SEPARATOR . 'controls' . DIRECTORY_SEPARATOR . $control . DIRECTORY_SEPARATOR . $action . '.latte';
	}



	private function extractPresenterModules(Presenter $presenter)
	{
		$fullyQuantified = $presenter->getAction(true);
		$parts = explode(":", $fullyQuantified);
		unset($parts[0]); // remove first :
		unset($parts[count($parts)]); // unset action, using function argument
		return array_values($parts);
	}



}

<?php

namespace Echo511\Experior\Domain;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Echo511\Experior\Domain\Answer;
use Echo511\Experior\Domain\QuestionTag;
use Echo511\Experior\Domain\User;
use Nette\Object;

/**
 * @ORM\Entity
 * 
 * @author Nikolas Tsiongas
 */
class Question extends Object
{

	const classname = __CLASS__;

	/**
	 * @var int
	 * 
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	private $id;

	/**
	 * @var Question
	 * 
	 * @ORM\ManyToOne(targetEntity="Question", inversedBy="versions", cascade={"persist"})
	 */
	private $firstVersion;

	/**
	 * @var Question
	 * 
	 * @ORM\ManyToOne(targetEntity="Question")
	 */
	private $lastVersion;

	/**
	 * @var Collection
	 * 
	 * @ORM\OneToMany(targetEntity="Question", mappedBy="firstVersion", cascade={"persist"})
	 */
	private $versions;

	/**
	 * @var string
	 * 
	 * @ORM\Column(type="string")
	 */
	private $text;

	/**
	 * @var Answer[]
	 * 
	 * @ORM\ManyToMany(targetEntity="Answer")
	 * @ORM\JoinTable(
	 * 	inverseJoinColumns={
	 * 		@ORM\JoinColumn(referencedColumnName="text"),
	 * 		@ORM\JoinColumn(referencedColumnName="is_correct"),
	 * 	}
	 * )
	 */
	private $answers;

	/**
	 * @ORM\ManyToMany(targetEntity="QuestionTag", inversedBy="questions")
	 * @ORM\JoinTable(
	 * 	inverseJoinColumns={@ORM\JoinColumn(referencedColumnName="title")}
	 * )
	 */
	private $tags;

	/**
	 * @var QuestionsImport
	 * 
	 * @ORM\ManyToOne(targetEntity="QuestionsImport", inversedBy="questions")
	 */
	private $questionsImport;

	/**
	 * @var DateTime
	 * 
	 * @ORM\Column(type="datetime")
	 */
	private $createdAt;

	/**
	 * @var User
	 * 
	 * @ORM\ManyToOne(targetEntity="User")
	 */
	private $createdBy;

	public function __construct()
	{
		$this->versions = new ArrayCollection([$this]);
		$this->answers = new ArrayCollection;
		$this->tags = new ArrayCollection;
		$this->createdAt = new DateTime;
	}



	public function getId()
	{
		return $this->id;
	}



	public function getFirstVersion()
	{
		if ($this->firstVersion === null) {
			return $this;
		}
		return $this->firstVersion;
	}



	public function getLastVersion()
	{
		if ($this->lastVersion === null) {
			return $this;
		}
		return $this->lastVersion;
	}



	public function getVersions()
	{
		if ($this->firstVersion === null) {
			return $this->versions;
		}
		return $this->firstVersion->getVersions();
	}



	public function getText()
	{
		return $this->text;
	}



	public function getAnswers()
	{
		return $this->answers;
	}



	public function getTags()
	{
		return $this->tags;
	}



	public function getQuestionsImport()
	{
		return $this->questionsImport;
	}



	public function getCreatedAt()
	{
		return $this->createdAt;
	}



	public function getCreatedBy()
	{
		return $this->createdBy;
	}



	public function isFirst()
	{
		return $this->getFirstVersion() === $this;
	}



	public function isLast()
	{
		return $this->getLastVersion() === $this;
	}



	public function isCurrent()
	{
		return $this->getLastVersion() === $this;
	}



	public function hasCurrentTextAndAnswers()
	{
		return ($this->getText() == $this->getLastVersion()->getText()) && ($this->getAnswers()->toArray() == $this->getLastVersion()->getAnswers()->toArray());
	}



	/**
	 * INTERNAL function. Use addVersion() instead.
	 * 
	 * @param Question $version
	 */
	public function setFirstVersion(Question $version)
	{
		$this->firstVersion = $version;
	}



	/**
	 * INTERNAL function. Use addVersion() instead.
	 * 
	 * @param Question $version
	 */
	public function setLastVersion(Question $version)
	{
		$this->lastVersion = $version;
	}



	public function addVersion()
	{
		if ($this->firstVersion === null) {
			$newVersion = new Question;
			$newVersion->setAnswers(clone $this->getLastVersion()->getAnswers())
				->setTags(clone $this->getLastVersion()->getTags())
				->setText($this->getLastVersion()->getText())
				->setQuestionsImport($this->getLastVersion()->getQuestionsImport());
			$this->lastVersion = $newVersion;
			foreach ($this->versions as $version) {
				$version->setLastVersion($newVersion);
			}
			$this->versions->add($newVersion);
			$newVersion->setFirstVersion($this);
			return $newVersion;
		}

		return $this->firstVersion->addVersion();
	}



	public function setText($text)
	{
		$this->text = $text;
		return $this;
	}



	public function setAnswers(Collection $answers)
	{
		$this->answers = $answers;
		return $this;
	}



	public function addAnswer(Answer $answer)
	{
		if (!$this->answers->contains($answer)) {
			$this->answers->add($answer);
		}
		return $this;
	}



	public function removeAnswer(Answer $answer)
	{
		if ($this->answers->contains($answer)) {
			$this->answers->removeElement($answer);
		}
		return $this;
	}



	public function setTags($tags)
	{
		$this->tags = $tags;
		return $this;
	}



	public function addTag(QuestionTag $tag)
	{
		if (!$this->tags->contains($tag)) {
			$this->tags->add($tag);
			$tag->addQuestion($this);
		}
		return $this;
	}



	public function removeTag(QuestionTag $tag)
	{
		if ($this->tags->contains($tag)) {
			$this->tags->removeElement($tag);
			$tag->removeQuestion($this);
		}
		return $this;
	}



	public function setQuestionsImport(QuestionsImport $questionsImport)
	{
		$this->questionsImport = $questionsImport;
		return $this;
	}



	public function setCreatedAt(DateTime $createdAt)
	{
		$this->createdAt = $createdAt;
		return $this;
	}



	public function setCreatedBy(User $createdBy)
	{
		$this->createdBy = $createdBy;
		return $this;
	}



}

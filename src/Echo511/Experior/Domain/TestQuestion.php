<?php

namespace Echo511\Experior\Domain;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Echo511\Experior\Domain\Answer;
use Echo511\Experior\Domain\Question;
use Echo511\Experior\Domain\TestProgress;
use Nette\Object;

/**
 * @ORM\Entity
 * 
 * @author Nikolas Tsiongas
 */
class TestQuestion extends Object
{

	const classname = __CLASS__;

	/**
	 * @var int
	 * 
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	private $id;

	/**
	 * @var TestProgress
	 * 
	 * @ORM\ManyToOne(targetEntity="TestProgress", inversedBy="testQuestions")
	 */
	private $testProgress;

	/**
	 * @var Question
	 * 
	 * @ORM\ManyToOne(targetEntity="Question")
	 */
	private $question;

	/**
	 * @var Answer[]
	 * 
	 * @ORM\ManyToMany(targetEntity="Answer")
	 * @ORM\JoinTable(
	 * 	inverseJoinColumns={
	 * 		@ORM\JoinColumn(referencedColumnName="text"),
	 * 		@ORM\JoinColumn(referencedColumnName="is_correct"),
	 * 	}
	 * )
	 */
	private $answered;

	/**
	 * @var boolean
	 * 
	 * @ORM\Column(type="boolean", nullable=true)
	 */
	private $isCorrectlyAnswered;

	/**
	 * @var DateTime
	 * 
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $answeredAt;

	public function __construct()
	{
		$this->answered = new ArrayCollection;
	}



	public function getId()
	{
		return $this->id;
	}



	public function getTestProgress()
	{
		return $this->testProgress;
	}



	public function getQuestion()
	{
		return $this->question;
	}



	public function isCurrent()
	{
		return $this->getQuestion()->isCurrent();
	}



	public function hasCurrentTextAndAnswers()
	{
		return $this->getQuestion()->hasCurrentTextAndAnswers();
	}



	public function getText()
	{
		return $this->getQuestion()->getText();
	}



	public function getAnswers()
	{
		return $this->question->getAnswers();
	}



	public function getAnswered()
	{
		return $this->answered;
	}



	public function getIsCorrectlyAnswered()
	{
		return $this->isCorrectlyAnswered;
	}



	public function getAnsweredAt()
	{
		return $this->answeredAt;
	}



	public function setTestProgress(TestProgress $testProgress = null)
	{
		if ($testProgress === null) {
			$this->testProgress = null;
			return $this;
		}

		if ($this->testProgress instanceof TestProgress && $this->testProgress !== $testProgress) {
			$this->testProgress->removeTestQuestion($this);
		}

		$this->testProgress = $testProgress;
		$testProgress->addTestQuestion($this);
		return $this;
	}



	public function setQuestion(Question $question)
	{
		$this->question = $question;
		return $this;
	}



	public function setAnsweredAt(DateTime $answeredAt)
	{
		$this->answeredAt = $answeredAt;
		return $this;
	}



	/**
	 * Was this answer choosen?
	 * 
	 * @param Answer $answer Description
	 * @return boolean
	 */
	public function hasAnswered(Answer $answer)
	{
		return $this->answered->contains($answer);
	}



	/**
	 * Answer question.
	 * *** @param int[] $answered IDs of answers
	 */
	public function answer(Collection $answered)
	{
		$this->answered = $answered;
		$this->answeredAt = new DateTime;
		$this->isCorrectlyAnswered = $this->calculateCorrectness();
		$this->testProgress->checkIfCompleted();
	}



	/**
	 * Decides whether has the answer been answered correctly or not.
	 * 
	 * @return boolean
	 */
	protected function calculateCorrectness()
	{
		foreach ($this->answered as $answer) {
			if (!$answer->getIsCorrect()) {
				return false;
			}
		}

		foreach ($this->getAnswers() as $answer) {
			if ($answer->getIsCorrect() && !$this->answered->contains($answer)) {
				return false;
			}
		}

		return true;
	}



}

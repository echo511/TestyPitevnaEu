<?php

namespace Echo511\Experior\Domain;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Nette\Object;

/**
 * @ORM\Entity
 * 
 * @author Nikolas Tsiongas
 */
class QuestionsImport extends Object
{

	const classname = __CLASS__;

	/**
	 * @var int
	 * 
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	private $id;

	/**
	 * @var Question[]
	 * 
	 * @ORM\OneToMany(targetEntity="Question", mappedBy="questionsImport", cascade={"persist"})
	 */
	private $questions;

	/**
	 * @var DateTime
	 * 
	 * @ORM\Column(type="datetime")
	 */
	private $createdAt;

	/**
	 * @var User
	 * 
	 * @ORM\ManyToOne(targetEntity="User")
	 */
	private $createdBy;

	public function __construct()
	{
		$this->questions = new ArrayCollection;
		$this->createdAt = new DateTime;
	}



	public function getId()
	{
		return $this->id;
	}



	public function getQuestions()
	{
		return $this->questions;
	}



	public function getCreatedAt()
	{
		return $this->createdAt;
	}



	public function getCreatedBy()
	{
		return $this->createdBy;
	}



	public function setQuestions($questions)
	{
		foreach ($questions as $question) {
			$question->setQuestionsImport($this);
		}
		$this->questions = $questions;
		return $this;
	}



	public function setCreatedAt(DateTime $createdAt)
	{
		$this->createdAt = $createdAt;
		return $this;
	}



	public function setCreatedBy(User $createdBy)
	{
		$this->createdBy = $createdBy;
		return $this;
	}



}

<?php

namespace Echo511\Experior\Domain;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Nette\Object;
use Nette\Security\Passwords;

/**
 * @ORM\Entity
 * 
 * @author Nikolas Tsiongas
 */
class User extends Object
{

	const classname = __CLASS__;

	/**
	 * @var int
	 * 
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	private $id;

	/**
	 * @var string
	 * 
	 * @ORM\Column(type="string", nullable=true)
	 */
	private $username;

	/**
	 * @var string
	 * 
	 * @ORM\Column(type="string", length=255)
	 */
	private $email;

	/**
	 * @var string
	 * 
	 * @ORM\Column(type="string", nullable=true)
	 */
	private $alias;

	/**
	 * @var string
	 * 
	 * @ORM\Column(type="string", nullable=true)
	 */
	private $password;

	/**
	 * @var string
	 * 
	 * @ORM\Column(type="string", nullable=true)
	 */
	private $facebookId;

	/**
	 * @var string
	 * 
	 * @ORM\Column(type="string", nullable=true)
	 */
	private $facebookAccessToken;

	/**
	 * @var DateTime
	 * 
	 * @ORM\Column(type="datetime")
	 */
	private $createdAt;

	public function __construct()
	{
		$this->createdAt = new DateTime;
	}



	public function getId()
	{
		return $this->id;
	}



	public function getUsername()
	{
		return $this->username;
	}



	public function getEmail()
	{
		return $this->email;
	}



	public function getAlias()
	{
		return $this->alias;
	}



	public function getFacebookId()
	{
		return $this->facebookId;
	}



	public function getFacebookAccessToken()
	{
		return $this->facebookAccessToken;
	}



	public function getCreatedAt()
	{
		return $this->createdAt;
	}



	public function isPasswordCorrect($password)
	{
		return Passwords::verify($password, $this->password);
	}



	public function setUsername($username)
	{
		$this->username = $username;
		return $this;
	}



	public function setEmail($email)
	{
		$this->email = $email;
		return $this;
	}



	public function setAlias($alias)
	{
		$this->alias = $alias;
		return $this;
	}



	public function setPassword($password)
	{
		$this->password = Passwords::hash($password);
		return $this;
	}



	public function setFacebookId($facebookId)
	{
		$this->facebookId = $facebookId;
		return $this;
	}



	public function setFacebookAccessToken($facebookAccessToken)
	{
		$this->facebookAccessToken = $facebookAccessToken;
		return $this;
	}



	public function setCreatedAt(DateTime $createdAt)
	{
		$this->createdAt = $createdAt;
		return $this;
	}



}

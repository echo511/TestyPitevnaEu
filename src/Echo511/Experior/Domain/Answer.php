<?php

namespace Echo511\Experior\Domain;

use Doctrine\ORM\Mapping as ORM;
use Nette\Object;

/**
 * @ORM\Entity
 * 
 * @author Nikolas Tsiongas
 */
class Answer extends Object
{

	const classname = __CLASS__;

	/**
	 * @var string
	 * 
	 * @ORM\Id
	 * @ORM\Column(type="string")
	 */
	private $text;

	/**
	 * @var boolean
	 * 
	 * @ORM\Id
	 * @ORM\Column(type="boolean")
	 */
	private $isCorrect;

	public function getText()
	{
		return $this->text;
	}



	public function getIsCorrect()
	{
		return $this->isCorrect;
	}



	public function setText($text)
	{
		$this->text = $text;
		return $this;
	}



	public function setIsCorrect($isCorrect)
	{
		$this->isCorrect = $isCorrect;
		return $this;
	}



}

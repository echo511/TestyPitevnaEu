<?php

namespace Echo511\Experior\Domain;

use Doctrine\ORM\Mapping as ORM;
use Echo511\Experior\Domain\User;
use Nette\Object;

/**
 * @ORM\Entity
 * 
 * @author Nikolas Tsiongas
 */
class QuestionPriority extends Object
{

	const classname = __CLASS__;

	/**
	 * @var Question
	 * 
	 * @ORM\Id
	 * @ORM\ManyToOne(targetEntity="Question")
	 */
	private $firstVersionOfQuestion;

	/**
	 * @var User
	 * 
	 * @ORM\Id
	 * @ORM\ManyToOne(targetEntity="User")
	 */
	private $user;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $lastAnswerCorrectness;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $correctnessRatio;

	function __construct()
	{
		$this->correctnessRatio = 0;
	}



	public function getFirstVersionOfQuestion()
	{
		return $this->firstVersionOfQuestion;
	}



	public function getUser()
	{
		return $this->user;
	}



	public function getLastAnswerCorrectness()
	{
		return $this->lastAnswerCorrectness;
	}



	public function getCorrectnessRatio()
	{
		return $this->correctnessRatio;
	}



	public function setFirstVersionOfQuestion(Question $firstVersionOfQuestion)
	{
		$this->firstVersionOfQuestion = $firstVersionOfQuestion;
		return $this;
	}



	public function setUser(User $user)
	{
		$this->user = $user;
		return $this;
	}



	public function setLastAnswerCorrectness($lastAnswerCorrectness)
	{
		$this->lastAnswerCorrectness = $lastAnswerCorrectness;
		return $this;
	}



	public function setCorrectnessRatio($correctnessRatio)
	{
		$this->correctnessRatio = $correctnessRatio;
		return $this;
	}



}

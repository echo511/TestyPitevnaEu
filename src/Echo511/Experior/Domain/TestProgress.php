<?php

namespace Echo511\Experior\Domain;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\PersistentCollection;
use Echo511\Experior\Criteria\TestQuestionCriteria;
use Nette\Object;

/**
 * @ORM\Entity
 * 
 * @author Nikolas Tsiongas
 */
class TestProgress extends Object
{

	const classname = __CLASS__;

	/**
	 * @var int
	 * 
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	private $id;

	/**
	 * @var TestQuestion[]
	 * 
	 * @ORM\OneToMany(targetEntity="TestQuestion", mappedBy="testProgress", cascade={"persist"})
	 */
	private $testQuestions;

	/**
	 * @var boolean
	 * 
	 * @ORM\Column(type="integer")
	 */
	private $isComplete;

	/**
	 * @var DateTime
	 * 
	 * @ORM\Column(type="datetime")
	 */
	private $createdAt;

	/**
	 * @var User
	 * 
	 * @ORM\ManyToOne(targetEntity="User")
	 */
	private $createdBy;

	/**
	 *
	 * @ORM\Column(type="string", nullable=true)
	 */
	private $sourceType;

	/**
	 *
	 * @ORM\Column(type="integer", nullable=true)
	 */
	private $sourceId;

	/**
	 *
	 * @ORM\Column(type="array", nullable=true)
	 */
	private $sourceData;

	/**
	 * @param Question[] $questions
	 * @param User $createdBy
	 */
	function __construct()
	{
		$this->createdAt = new DateTime;
		$this->isComplete = false;
		$this->testQuestions = new ArrayCollection;
	}



	public function getId()
	{
		return $this->id;
	}



	public function getQuestions()
	{
		$questions = new ArrayCollection;
		foreach ($this->getTestQuestions() as $testQuestion) {
			$questions[] = $testQuestion->getQuestion();
		}
		return $questions;
	}



	public function getTestQuestions()
	{
		return $this->testQuestions;
	}



	public function getTestQuestion($id)
	{
		$result = $this->testQuestions->matching(TestQuestionCriteria::create()
				->filterId($id)
		);

		if ($result->count() == 0) {
			throw new NoResultException;
		}

		return $result->first();
	}



	public function getIsComplete()
	{
		return $this->isComplete;
	}



	public function getScore()
	{
		if ($this->testQuestions->count() == 0) {
			return null;
		}

		$correct = 0;
		foreach ($this->testQuestions as $testQuestion) {
			if ($testQuestion->getIsCorrectlyAnswered()) {
				$correct++;
			}
		}
		return round($correct / $this->testQuestions->count(), 4);
	}



	public function getCreatedAt()
	{
		return $this->createdAt;
	}



	public function getCreatedBy()
	{
		return $this->createdBy;
	}



	public function getSourceType()
	{
		return $this->sourceType;
	}



	public function getSourceId()
	{
		return $this->sourceId;
	}



	public function getSourceData()
	{
		return $this->sourceData;
	}



	public function getUnansweredTestQuestions()
	{
		if ($this->testQuestions instanceof PersistentCollection) {
			$this->testQuestions->initialize();
		}

		return $this->testQuestions->matching(TestQuestionCriteria::create()
					->filterUnanswered()
		);
	}



	public function getRandomUnansweredTestQuestion()
	{
		$unansweredQuestions = $this->getUnansweredTestQuestions();
		if ($unansweredQuestions->count() == 0) {
			throw new NoResultException;
		}
		$questions = $unansweredQuestions->toArray();
		return $questions[array_rand($questions)];
	}



	public function checkIfCompleted()
	{
		$this->isComplete = ($this->getUnansweredTestQuestions()->count() == 0);
		return $this;
	}



	public function setTestQuestions(Collection $testQuestions)
	{
		foreach ($testQuestions as $testQuestion) {
			$testQuestion->setTestProgress($this);
		}
		$this->testQuestions = $testQuestions;
		return $this;
	}



	public function addTestQuestion(TestQuestion $testQuestion)
	{
		if (!$this->testQuestions->contains($testQuestion)) {
			$this->testQuestions->add($testQuestion);
			$testQuestion->setTestProgress($this);
		}
		return $this;
	}



	public function removeTestQuestion(TestQuestion $testQuestion)
	{
		if ($this->testQuestions->contains($testQuestion)) {
			$this->testQuestions->removeElement($testQuestion);
			$testQuestion->setTestProgress(null);
		}
		return $this;
	}



	public function setIsComplete($isComplete)
	{
		$this->isComplete = $isComplete;
		return $this;
	}



	public function setCreatedAt(DateTime $createdAt)
	{
		$this->createdAt = $createdAt;
		return $this;
	}



	public function setCreatedBy(User $createdBy)
	{
		$this->createdBy = $createdBy;
		return $this;
	}



	public function setSourceType($sourceType)
	{
		$this->sourceType = $sourceType;
		return $this;
	}



	public function setSourceId($sourceId)
	{
		$this->sourceId = $sourceId;
		return $this;
	}



	public function setSourceData($sourceData)
	{
		$this->sourceData = $sourceData;
		return $this;
	}



}

<?php

namespace Echo511\Experior\Domain;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Nette\Object;

/**
 * @ORM\Entity
 * 
 * @author Nikolas Tsiongas
 */
class QuestionTag extends Object
{

	const classname = __CLASS__;

	/**
	 * @ORM\Id
	 * @ORM\Column(type="string")
	 */
	private $title;

	/**
	 * @ORM\Column(type="string", unique=true)
	 */
	private $webalize;

	/**
	 * @ORM\ManyToMany(targetEntity="Question", mappedBy="tags", cascade={"all"}, fetch="EXTRA_LAZY")
	 */
	private $questions;

	public function __construct()
	{
		$this->questions = new ArrayCollection;
	}



	public function getTitle()
	{
		return $this->title;
	}



	public function getWebalize()
	{
		return $this->webalize;
	}



	public function getQuestions()
	{
		return $this->questions;
	}



	public function setTitle($title)
	{
		$this->title = $title;
		return $this;
	}



	public function setWebalize($webalize)
	{
		$this->webalize = $webalize;
		return $this;
	}



	public function addQuestion(Question $question)
	{
		if (!$this->questions->contains($question)) {
			$this->questions->add($question);
			$question->addTag($this);
		}
		return $this;
	}



	public function removeQuestion(Question $question)
	{
		if ($this->questions->contains($question)) {
			$this->questions->removeElement($question);
			$question->removeTag($this);
		}
		return $this;
	}



}

<?php

namespace Echo511\Experior\Domain;

use Nette\Object;

/**
 * @author Nikolas Tsiongas
 */
class TestProgressSource extends Object
{

	private $id;
	private $type;
	private $data;

	public function getId()
	{
		return $this->id;
	}



	public function getType()
	{
		return $this->type;
	}



	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}



	public function setType($type)
	{
		$this->type = $type;
		return $this;
	}



}

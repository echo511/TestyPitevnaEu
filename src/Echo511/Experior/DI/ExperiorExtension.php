<?php

namespace Echo511\Experior\DI;

use Echo511\Gaufrette\DI\IAssetsProvider;
use Kdyby\Doctrine\DI\IEntityProvider;
use Nette\DI\CompilerExtension;

/**
 * Register experior into nette's DI.
 * 
 * @author Nikolas Tsiongas
 */
class ExperiorExtension extends CompilerExtension implements IAssetsProvider, IEntityProvider
{

	const classname = __CLASS__;

	/**
	 * Use services from config.neon
	 */
	public function loadConfiguration()
	{
		$this->compiler->parseServices($this->containerBuilder, $this->loadFromFile(__DIR__ . '/config.neon'));
	}



	/**
	 * Disable nette's user storage autowiring.
	 */
	public function beforeCompile()
	{
		$this->containerBuilder->getDefinition('nette.userStorage')
			->setAutowired(false);
	}



	/**
	 * Implement IAssetsProvider.
	 * 
	 * @return string
	 */
	public function getDataDirectory()
	{
		return __DIR__ . '/../assets';
	}



	/**
	 * Implement IEntityProvider
	 * 
	 * @return array
	 */
	public function getEntityMappings()
	{
		return [
		    'Echo511\Experior\Domain' => __DIR__ . '/../Domain'
		];
	}



}

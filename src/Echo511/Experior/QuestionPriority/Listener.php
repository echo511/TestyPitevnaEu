<?php

namespace Echo511\Experior\QuestionPriority;

use Doctrine\ORM\Event\OnFlushEventArgs;
use Echo511\Experior\Domain\TestQuestion;
use Echo511\Experior\Manipulator\QuestionPriorityManipulator;
use Kdyby\Events\Subscriber;
use Nette\Object;
use Nette\Security\User;

class Listener extends Object implements Subscriber
{

	/** @var User */
	private $user;

	/** @var QuestionPriorityManipulator */
	private $questionPriorityManipulator;

	function __construct(User $user, QuestionPriorityManipulator $questionPriorityManipulator)
	{
		$this->user = $user;
		$this->questionPriorityManipulator = $questionPriorityManipulator;
	}



	public function getSubscribedEvents()
	{
		return [
		    'Doctrine\ORM\EntityManager::onFlush'
		];
	}



	public function onFlush(OnFlushEventArgs $args)
	{
		$recompute = false;
		$uow = $args->getEntityManager()->getUnitOfWork();
		foreach ($uow->getScheduledEntityUpdates() as $entity) {
			if ($entity instanceof TestQuestion) {
				$questionPriority = $this->questionPriorityManipulator->getQuestionPriority($entity->getQuestion(), $this->user->getIdentity());

				if ($entity->getIsCorrectlyAnswered() && $questionPriority->getCorrectnessRatio() < 3) {
					$questionPriority->setCorrectnessRatio($questionPriority->getCorrectnessRatio() + 1);
				} elseif (!$entity->getIsCorrectlyAnswered() && $questionPriority->getCorrectnessRatio() > -3) {
					$questionPriority->setCorrectnessRatio($questionPriority->getCorrectnessRatio() - 1);
				}

				$questionPriority->setLastAnswerCorrectness($entity->getIsCorrectlyAnswered());
				$uow->persist($questionPriority);
				$recompute = true;
			}
		}
		if ($recompute) {
			$uow->computeChangeSets();
		}
	}



}

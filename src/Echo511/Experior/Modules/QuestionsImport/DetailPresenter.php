<?php

namespace Echo511\Experior\Modules\QuestionsImport;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\NoResultException;
use Echo511\Experior\Control\IEditableQuestionControlFactory;
use Echo511\Experior\Domain\QuestionsImport;
use Echo511\Experior\Presenter\BasePresenter;
use Echo511\VisualPaginator\VisualPaginator;
use Nette\Application\UI\Control;
use Nette\Utils\Strings;

/**
 * Details of import.
 * 
 * @author Nikolas Tsiongas
 */
class DetailPresenter extends BasePresenter
{

	/** @var DetailFacade @inject */
	public $detailFacade;

	/** @var IEditableQuestionControlFactory @inject */
	public $editableQuestionControlFactory;

	/** @var int @persistent */
	public $id;

	/** @var QuestionsImport */
	private $import;

	/** @var Collection */
	private $questions;

	//*********** Dependency ***********//
	//*********** Init ***********//

	/**
	 * Import id must be provided. If else redirect to list.
	 */
	public function startup()
	{
		parent::startup();
		if (!$this->id) {
			$this->flashMessage('Import nebyl vybrán.', 'error');
			$this->redirect('List:default');
		}

		try {
			$this->import = $this->detailFacade->getImport($this->id);
		} catch (NoResultException $e) {
			$this->flashMessage('Import nenalezen.', 'error');
			$this->redirect('List:default');
		}
	}



	//*********** Actions & Signals ***********//

	public function actionDefault()
	{
		$paginator = $this['questionsPaginator']->getPaginator();
		$paginator->setItemsPerPage(10);

		$this->questions = $this->detailFacade->getQuestions($this->import);
		$this->questions->applyPaginator($paginator);
	}



	/**
	 * Default render.
	 */
	public function renderDefault()
	{
		$this->template->import = $this->import;
		$this->template->questions = $this->questions;
		$this->template->tags = $this->detailFacade->getQuestionTags($this->import);
	}



	/**
	 * Start test with questions from import.
	 */
	public function handleStartTest()
	{
		$testProgress = $this->detailFacade->startTest($this->import);
		$this->redirect(':Experior:TestProgress:SingleQuestion:default', ['id' => $testProgress->getId()]);
	}



	/**
	 * Add tag to all questions.
	 */
	public function handleAddTag()
	{
		$title = $this->request->getPost()['value'];
		$this->detailFacade->addTag($this->import, $title);

		if ($this->isAjax()) {
			$this->redrawControl();
		} else {
			$this->redirect('this');
		}
	}



	//*********** Components ***********//

	/**
	 * Multiple questions support.
	 * 
	 * @param string $name
	 * @return Control
	 */
	public function createComponent($name)
	{
		if (Strings::startsWith($name, 'question_')) {
			list($component, $questionId) = explode('_', $name);
			return $this->editableQuestionControlFactory->create($this->detailFacade->getQuestion((int) $questionId));
		}
		return parent::createComponent($name);
	}



	/**
	 * Paginate questions in import.
	 * 
	 * @return VisualPaginator
	 */
	public function createComponentQuestionsPaginator()
	{
		if ($this->isAjax()) {
			$this->redrawControl('questions');
		}
		$paginator = (new VisualPaginator());
		$paginator->onShowPage[] = function() {
			$this->redirect('this');
		};
		$paginator->changePageLink = function($page) {
			return $this->link('this', ['questionsPaginator-page' => $page]);
		};
		return $paginator;
	}



}

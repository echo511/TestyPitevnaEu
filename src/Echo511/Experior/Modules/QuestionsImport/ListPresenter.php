<?php

namespace Echo511\Experior\Modules\QuestionsImport;

use Echo511\Experior\Presenter\BasePresenter;

/**
 * List imports.
 * 
 * @author Nikolas Tsiongas
 */
class ListPresenter extends BasePresenter
{

	/** @var ListFacade @inject */
	public $listFacade;

	public function renderDefault()
	{
		$this->template->imports = $this->listFacade->getImports();
	}



}

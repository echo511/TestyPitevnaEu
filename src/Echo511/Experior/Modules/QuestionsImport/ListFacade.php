<?php

namespace Echo511\Experior\Modules\QuestionsImport;

use Echo511\Experior\Domain\QuestionsImport;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\ResultSet;
use Nette\Object;

/**
 * Perform operations over questions imports.
 * 
 * @author Nikolas Tsiongas
 */
class ListFacade extends Object
{

	/** @var EntityManager */
	private $em;

	function __construct(EntityManager $em)
	{
		$this->em = $em;
	}



	/**
	 * Return all imports.
	 * 
	 * @return ResultSet
	 */
	public function getImports()
	{
		return $this->em->getDao(QuestionsImport::classname)
				->findAll();
	}



}

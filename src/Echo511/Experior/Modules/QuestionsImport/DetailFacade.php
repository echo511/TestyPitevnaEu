<?php

namespace Echo511\Experior\Modules\QuestionsImport;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\NoResultException;
use Echo511\Experior\Criteria\QuestionCriteria;
use Echo511\Experior\Domain\Question;
use Echo511\Experior\Domain\QuestionsImport;
use Echo511\Experior\Domain\QuestionTag;
use Echo511\Experior\Domain\TestProgress;
use Echo511\Experior\Domain\TestProgressSource;
use Echo511\Experior\Manipulator\QuestionManipulator;
use Echo511\Experior\Manipulator\TestProgressManipulator;
use Echo511\Experior\Query\QuestionsImportDetailQuery;
use Echo511\Experior\Query\QuestionsImportTagQuery;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\ResultSet;
use Nette\Object;
use Nette\Security\User;

/**
 * @author Nikolas Tsiongas
 */
class DetailFacade extends Object
{

	/** @var EntityManager */
	private $em;

	/** @var QuestionManipulator */
	private $questionManipulator;

	/** @var TestProgressManipulator */
	private $testProgressManipulator;

	/** @var User */
	private $user;

	function __construct(EntityManager $em, QuestionManipulator $questionManipulator, TestProgressManipulator $testProgressManipulator, User $user)
	{
		$this->em = $em;
		$this->questionManipulator = $questionManipulator;
		$this->testProgressManipulator = $testProgressManipulator;
		$this->user = $user;
	}



	/**
	 * Unsaturated import.
	 * 
	 * @param int $id
	 * @return QuestionsImport
	 * @throws NoResultException
	 */
	public function getImport($id)
	{
		$result = $this->em->getDao(QuestionsImport::classname)
			->find($id);

		if ($result === null) {
			throw new NoResultException;
		}

		return $result;
	}



	/**
	 * Get question from subset. Used to check if question belongs to import.
	 * 
	 * @param Collection $questions
	 * @param int $id
	 * @return Question
	 * @throws NoResultException
	 */
	public function getQuestion($id)
	{
		$result = $this->em->getDao(Question::classname)
			->find($id);

		if ($result === null) {
			throw new NoResultException;
		}

		return $result;
	}



	/**
	 * Saturated questions belonging to the import.
	 * 
	 * @param QuestionsImport $import
	 * @return ResultSet
	 */
	public function getQuestions(QuestionsImport $import)
	{
		return $this->em->getDao(Question::classname)
				->fetch(new QuestionsImportDetailQuery($import));
	}



	/**
	 * Tags belonging to questions.
	 * 
	 * @return ResultSet
	 */
	public function getQuestionTags(QuestionsImport $import)
	{
		return $this->em->getDao(QuestionTag::classname)
				->fetch(new QuestionsImportTagQuery($import));
	}



	/**
	 * Add tag to all questions in import.
	 * 
	 * @param QuestionsImport $import
	 * @param string $tagTitle
	 */
	public function addTag(QuestionsImport $import, $tagTitle)
	{
		foreach ($this->getQuestions($import) as $question) {
			$this->questionManipulator->addTag($this->addQuestionVersion($question), $tagTitle);
			$this->em->persist($question);
		}
		$this->em->flush();
	}



	/**
	 * Start test from import.
	 * 
	 * @param QuestionsImport $import
	 * @param Collection $questions
	 * @return TestProgress
	 */
	public function startTest(QuestionsImport $import)
	{
		$source = (new TestProgressSource())
			->setId($import->getId())
			->setType('questionsImport');
		$testProgress = $this->testProgressManipulator->createTestProgress($import->getQuestions(), $source)
			->setCreatedBy($this->user->getIdentity());
		$this->em->persist($testProgress);
		$this->em->flush();
		return $testProgress;
	}



	/**
	 * Add version of question and set the author.
	 * 
	 * @param Question $question
	 * @return Question
	 */
	protected function addQuestionVersion(Question $question)
	{
		return $question->addVersion()
				->setCreatedBy($this->user->getIdentity());
	}



}

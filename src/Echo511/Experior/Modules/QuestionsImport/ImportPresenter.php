<?php

namespace Echo511\Experior\Modules\QuestionsImport;

use Echo511\Experior\Presenter\BasePresenter;
use Nette\Application\UI\Form;

/**
 * Questions importing.
 * 
 * @author Nikolas Tsiongas
 */
class ImportPresenter extends BasePresenter
{

	/** @var ImportFacade @inject */
	public $importFacade;

	protected function createComponentFormImport()
	{
		$form = new Form;
		$form->addRadioList('method', null, ['xml' => 'XML formát'])
			->setDefaultValue('xml')
			->addRule(Form::FILLED, 'Zvolte metodu importu.');
		$form->addUpload('file')
			->addRule(Form::FILLED, 'Musíte zvolit soubor k nahrání.');
		$form->addSubmit('submit');
		$form->onSuccess[] = function(Form $form, $values) {
			if ($values->file->isOk()) {
				try {
					$string = file_get_contents($values->file->getTemporaryFile());
					$import = $this->importFacade->importXML($string);
					$this->redirect('Detail:default', ['id' => $import->getId()]);
				} catch (ImportException $e) {
					$this->flashMessage('Import se nezdařil.', 'danger');
				}
			}
		};
		return $form;
	}



}

<?php

namespace Echo511\Experior\Modules\QuestionsImport;

use Exception;

/**
 * @author Nikolas Tsiongas
 */
class ImportException extends Exception
{
	
}

<?php

namespace Echo511\Experior\Modules\QuestionsImport;

use Echo511\Experior\Import\QuestionsImporter\Importer;
use Echo511\Experior\Import\QuestionsImporter\Parser\XmlParser;
use Echo511\Experior\Import\QuestionsImporter\ParserException;
use Nette\Object;
use Nette\Security\User;
use Tracy\Debugger;

/**
 * @author Nikolas Tsiongas
 */
class ImportFacade extends Object
{

	/** @var Importer */
	private $importer;

	/** @var XmlParser */
	private $xmlParser;

	/** @var User */
	private $user;

	function __construct(Importer $importer, XmlParser $xmlParser, User $user)
	{
		$this->importer = $importer;
		$this->xmlParser = $xmlParser;
		$this->user = $user;
	}



	/**
	 * Import from xml string.
	 * 
	 * @param string $string
	 * @return Import
	 * @throws ImportException
	 */
	public function importXML($string)
	{
		try {
			$questions = $this->xmlParser->parse($string);
			return $this->importer->import($questions, $this->user->getIdentity());
		} catch (ParserException $e) {
			Debugger::log($e);
			throw new ImportException;
		}
	}



}

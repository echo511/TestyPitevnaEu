<?php

namespace Echo511\Experior\Modules\TestProgress;

use Echo511\Experior\Domain\TestProgress;
use Echo511\Experior\Domain\TestProgressSource;
use Echo511\Experior\Manipulator\QuestionManipulator;
use Echo511\Experior\Manipulator\TestProgressManipulator;
use Kdyby\Doctrine\EntityManager;
use Nette\Object;
use Nette\Security\User;

/**
 * @author Nikolas Tsiongas
 */
class DetailFacade extends Object
{

	/** @var EntityManager */
	private $em;

	/** @var QuestionManipulator */
	private $questionManipulator;

	/** @var TestProgressManipulator */
	private $testProgressManipulator;

	/** @var User */
	private $user;

	function __construct(EntityManager $em, QuestionManipulator $questionManipulator, TestProgressManipulator $testProgressManipulator, User $user)
	{
		$this->em = $em;
		$this->questionManipulator = $questionManipulator;
		$this->testProgressManipulator = $testProgressManipulator;
		$this->user = $user;
	}



	/**
	 * Restart the test.
	 * 
	 * @param TestProgress $progress
	 * @return TestProgress
	 */
	public function startTest(TestProgress $progress)
	{
		$source = (new TestProgressSource())
			->setId($progress->getId())
			->setType('testProgress');
		$testProgress = $this->testProgressManipulator->createTestProgress($progress->getQuestions(), $source)
			->setCreatedBy($this->user->getIdentity());
		$this->em->persist($testProgress);
		$this->em->flush();
		return $testProgress;
	}



}

<?php

namespace Echo511\Experior\Modules\TestProgress;

use Doctrine\Common\Collections\ArrayCollection;
use Echo511\Experior\Criteria\TestQuestionCriteria;
use Echo511\Experior\Domain\TestProgress;
use Echo511\Experior\Domain\TestQuestion;
use Kdyby\Doctrine\EntityManager;
use Nette\Object;

/**
 * @author Nikolas Tsiongas
 */
class SingleQuestionFacade extends Object
{

	/** @var EntityManager */
	private $em;

	function __construct(EntityManager $em)
	{
		$this->em = $em;
	}



	/**
	 * Get next unanswered test question. Order of test questions setted on test progress creation.
	 * 
	 * @param TestProgress $testProgress
	 * @return TestQuestion
	 */
	public function getUnansweredTestQuestion(TestProgress $testProgress)
	{
		return $this->em->getDao(TestQuestion::classname)
				->createQueryBuilder()
				->select('tq')
				->from(TestQuestion::classname, 'tq')
				->leftJoin('tq.testProgress', 'pr')
				->where('pr.id = :prid', $testProgress->getId())
				->andWhere('tq.answeredAt IS NULL')
				->setMaxResults(1)
				->getQuery()->getSingleResult();
	}



	/**
	 * Get count of answered questions.
	 * 
	 * @param TestProgress $testProgress
	 * @return int
	 */
	public function getQuestionsBehindCount(TestProgress $testProgress)
	{
		return $this->em->getDao(TestQuestion::classname)
				->matching(TestQuestionCriteria::create()
					->filterTestProgress($testProgress)
					->filterAnswered())
				->count();
	}



	/**
	 * Get count of unanswered questions.
	 * 
	 * @param TestProgress $testProgress
	 * @return int
	 */
	public function getQuestionsAheadCount(TestProgress $testProgress)
	{
		return $this->em->getDao(TestQuestion::classname)
				->matching(TestQuestionCriteria::create()
					->filterTestProgress($testProgress)
					->filterUnanswered())
				->count();
	}



	/**
	 * Answer question belonging to the test progress.
	 * 
	 * @param TestProgress $testProgress
	 * @param int $testQuestionId
	 * @param array $answers
	 */
	public function answer(TestProgress $testProgress, $testQuestionId, array $answers)
	{
		$testQuestion = $this->em->getDao(TestQuestion::classname)
				->createQueryBuilder()
				->select('tq')
				->from(TestQuestion::classname, 'tq')
				->leftJoin('tq.testProgress', 'pr')
				->where('pr.id = :prid', $testProgress->getId())
				->andWhere('tq.id = :tqid', $testQuestionId)
				->setMaxResults(1)
				->getQuery()->getSingleResult();

		$answered = new ArrayCollection;
		foreach ($answers as $answer) {
			$text = $answer['text'];
			$isCorrect = $answer['isCorrect'];

			foreach ($testQuestion->getAnswers() as $answer) {
				if ($answer->getText() == $text && $answer->getIsCorrect() == $isCorrect) {
					$answered[] = $answer;
				}
			}
		}
		$testQuestion->answer($answered);
		$this->em->persist($testProgress);
		$this->em->flush();
	}



}

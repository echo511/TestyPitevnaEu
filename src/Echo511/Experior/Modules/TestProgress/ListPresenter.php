<?php

namespace Echo511\Experior\Modules\TestProgress;

/**
 * List imports.
 * 
 * @author Nikolas Tsiongas
 */
class ListPresenter extends BasePresenter
{

	/** @var ListFacade @inject */
	public $listFacade;

	/**
	 * Render.
	 */
	public function renderDefault()
	{
		$this->template->completeTestProgresses = $this->listFacade->getCompleteTestProgresses();
		$this->template->incompleteTestProgresses = $this->listFacade->getIncompleteTestProgresses();
	}



}

<?php

namespace Echo511\Experior\Modules\TestProgress;

use Doctrine\ORM\NoResultException;
use Echo511\Experior\Domain\TestProgress;
use Echo511\Experior\Presenter\BasePresenter as BPresenter;

/**
 * Predecessor for all presenters in current module.
 * 
 * @author Nikolas Tsiongas
 */
abstract class BasePresenter extends BPresenter
{

	/** @var BaseFacade @inject */
	public $baseFacade;

	/** @var int @persistent */
	public $id;

	/** @var TestProgress */
	protected $testProgress;

	/**
	 * Check permissions.
	 */
	public function startup()
	{
		parent::startup();

		if ($this->getAction(true) != ':Experior:TestProgress:List:default' && $this->getAction(true) != ':Experior:TestProgress:New:default') {
			if (!$this->id) {
				$this->flashMessage('Test nebyl vybrán.', 'error');
				$this->redirect('List:default');
			}

			try {
				$this->testProgress = $this->baseFacade->getTestProgress($this->id);

				if ($this->testProgress->getCreatedBy() !== $this->user->getIdentity()) {
					$this->flashMessage('Test nebyl vytvořen vámi.');
					$this->redirect('List:default');
				}
			} catch (NoResultException $e) {
				$this->flashMessage('Test nenalezen.', 'error');
				$this->redirect('List:default');
			}
		}
	}



}

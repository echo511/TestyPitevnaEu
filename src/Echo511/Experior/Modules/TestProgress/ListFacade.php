<?php

namespace Echo511\Experior\Modules\TestProgress;

use Doctrine\Common\Collections\Collection;
use Echo511\Experior\Criteria\TestProgressCriteria;
use Echo511\Experior\Domain\TestProgress;
use Kdyby\Doctrine\EntityManager;
use Nette\Object;
use Nette\Security\User;

/**
 * @author Nikolas Tsiongas
 */
class ListFacade extends Object
{

	/** @var EntityManager */
	private $em;

	/** @var User */
	private $user;

	function __construct(EntityManager $em, User $user)
	{
		$this->em = $em;
		$this->user = $user;
	}



	/**
	 * @return Collection
	 */
	public function getCompleteTestProgresses()
	{
		return $this->em->getDao(TestProgress::classname)
				->matching(TestProgressCriteria::create()->filterComplete()->filterCreatedBy($this->user->getIdentity()));
	}



	/**
	 * @return Collection
	 */
	public function getIncompleteTestProgresses()
	{
		return $this->em->getDao(TestProgress::classname)
				->matching(TestProgressCriteria::create()->filterIncomplete()->filterCreatedBy($this->user->getIdentity()));
	}



}

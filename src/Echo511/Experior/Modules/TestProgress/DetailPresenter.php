<?php

namespace Echo511\Experior\Modules\TestProgress;

use Echo511\Experior\Control\IEditableTestQuestionControlFactory;
use Nette\Application\UI\Control;
use Nette\Utils\Strings;

/**
 * Test completion detail.
 * 
 * @author Nikolas Tsiongas
 */
class DetailPresenter extends BasePresenter
{

	/** @var DetailFacade @inject */
	public $detailFacade;

	/** @var IEditableTestQuestionControlFactory @inject */
	public $editableTestQuestionFactory;

	public function startup()
	{
		parent::startup();

		if (!$this->testProgress->getIsComplete()) {
			$this->flashMessage('Test nebyl vyplněn.');
			$this->redirect('SingleQuestion:default', ['id' => $this->id]);
		}
	}



	/**
	 * Restart test.
	 */
	public function handleStartTest()
	{
		$testProgress = $this->detailFacade->startTest($this->testProgress);
		$this->redirect(':Experior:TestProgress:SingleQuestion:default', ['id' => $testProgress->getId()]);
	}



	/**
	 * Render.
	 */
	public function renderDefault()
	{
		$this->template->testProgress = $this->testProgress;
	}



	/**
	 * Support multiple questions per page.
	 * 
	 * @param string $name
	 * @return Control
	 */
	protected function createComponent($name)
	{
		if (Strings::startsWith($name, 'testQuestion_')) {
			list($name, $id) = explode("_", $name);
			return $this->editableTestQuestionFactory->create($this->testProgress->getTestQuestion((int) $id));
		}
		return parent::createComponent($name);
	}



}

<?php

namespace Echo511\Experior\Modules\TestProgress;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query\Expr;
use Echo511\Experior\Domain\Question;
use Echo511\Experior\Domain\QuestionPriority;
use Echo511\Experior\Domain\QuestionTag;
use Echo511\Experior\Domain\TestProgress;
use Echo511\Experior\Domain\User;
use Echo511\Experior\Manipulator\TestProgressManipulator;
use Kdyby\Doctrine\EntityManager;
use Nette\Object;

class NewFacade extends Object
{

	/** @var EntityManager */
	private $em;

	/** @var TestProgressManipulator */
	private $testProgressManipulator;

	function __construct(EntityManager $em, TestProgressManipulator $testProgressManipulator)
	{
		$this->em = $em;
		$this->testProgressManipulator = $testProgressManipulator;
	}



	public function startTest($numberOfQuestions, $tags, User $user)
	{
		$expr = new Expr;
		$priorQueryBuilder = $this->em->getDao(QuestionPriority::classname)
			->createQueryBuilder()
			->select('fv.id', 'p.correctnessRatio')
			->from(QuestionPriority::classname, 'p')
			->leftJoin('p.firstVersionOfQuestion', 'fv')
			->leftJoin('fv.lastVersion', 'lv')
			->leftJoin('p.user', 'u')
			->andWhere($expr->eq('u.id', $user->getId()));
		if (count($tags) > 0) {
			$priorQueryBuilder
				->leftJoin('fv.tags', 'fv_t')
				->leftJoin('lv.tags', 'lv_t')
				->andWhere(
					$expr->orX(
						$expr->andX(
							$expr->in('fv_t.title', $tags), $expr->isNull('fv.lastVersion')
						), $expr->in('lv_t.title', $tags)
					)
			);
		}
		$priorQueryBuilder->addOrderBy('p.lastAnswerCorrectness', 'ASC')
			->addOrderBy('p.correctnessRatio', 'ASC')
			->setMaxResults($numberOfQuestions);

		$priorResult = $priorQueryBuilder->getQuery()->getResult();

		$prior = array_unique(array_filter(array_map(function($row) {
					if ($row['correctnessRatio'] < 3) {
						if ($row['id'] !== null) {
							return $row['id'];
						}
					}
				}, $priorResult)));

		$restrict = array_map(function($row) {
			return $row['id'];
		}, $priorResult);

		$needed = [];
		if (count($prior) < $numberOfQuestions) {
			$need = $numberOfQuestions - count($prior);

			$neededQueryBuilder = $this->em->getDao(QuestionPriority::classname)
				->createQueryBuilder()
				->select('q.id')
				->from(Question::classname, 'q')
				->andWhere($expr->isNull('q.lastVersion'))
				->setMaxResults($need);
			if (count($restrict) > 0) {
				$neededQueryBuilder
					->leftJoin('q.firstVersion', 'fv')
					->andWhere($expr->notIn('q.id', $restrict))
					->andWhere($expr->notIn('fv.id', $restrict));
			}
			if (count($tags) > 0) {
				$neededQueryBuilder
					->leftJoin('q.tags', 't')
					->andWhere($expr->in('t.title', $tags));
			}

			$needed = array_map(function($row) {
				return $row['id'];
			}, $neededQueryBuilder->getQuery()->getResult());
		}

		$questions = (array) $this->em->getDao(Question::classname)
				->findBy(['id' => array_merge($prior, $needed)]);

		$testProgress = $this->testProgressManipulator->createTestProgress(new ArrayCollection($questions));
		$testProgress->setCreatedBy($user);
		$testProgress->setSourceType('questionsTags');
		$testProgress->setSourceData([
		    'numberOfQuestions' => $numberOfQuestions,
		    'tags' => $tags
		]);

		$this->em->persist($testProgress);
		$this->em->flush();
		return $testProgress;
	}



	public function getTestProgress($id)
	{
		return $this->em->getDao(TestProgress::classname)
				->find($id);
	}



	public function getTags()
	{
		return $this->em->getDao(QuestionTag::classname)
				->findAll();
	}



}

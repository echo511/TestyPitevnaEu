<?php

namespace Echo511\Experior\Modules\TestProgress;

use Doctrine\ORM\NoResultException;
use Echo511\Experior\Domain\Answer;
use Echo511\Experior\Domain\TestQuestion;
use Nette\Application\UI\Form;

/**
 * Single question per page. On answer submit go to next question.
 * 
 * @author Nikolas Tsiongas
 */
class SingleQuestionPresenter extends BasePresenter
{

	/** @var SingleQuestionFacade @inject */
	public $singleQuestionFacade;

	/** @var TestQuestion */
	private $currentTestQuestion;

	/**
	 * Answer question.
	 * 
	 * @param Form $form
	 */
	public function answer(Form $form)
	{
		$testQuestionId = $form->getHttpData(Form::DATA_TEXT, 'testQuestionId');
		$encodedAnswers = $form->getHttpData(Form::DATA_TEXT, 'answers[]');
		$answers = [];
		foreach ($encodedAnswers as $encodedAnswer) {
			$answers[] = $this->decodeAnswer($encodedAnswer);
		}
		$this->singleQuestionFacade->answer($this->testProgress, $testQuestionId, $answers);

		if ($this->isAjax()) {
			$this->redrawControl();
		} else {
			$this->redirect('this');
		}
	}



	/**
	 * Choose random unanswered question.
	 */
	public function beforeRender()
	{
		try {
			$this->currentTestQuestion = $this->singleQuestionFacade->getUnansweredTestQuestion($this->testProgress);
		} catch (NoResultException $e) {
			$this->flashMessage('Test byl celý vyplněn.');
			$this->redirect('Detail:default', ['id' => $this->testProgress->getId()]);
		}
	}



	/**
	 * Render.
	 */
	public function renderDefault()
	{
		$this->template->encodeAnswer = function(Answer $answer) {
			return $this->encodeAnswer($answer);
		};
		$this->template->questionsBehindCount = $this->singleQuestionFacade->getQuestionsBehindCount($this->testProgress);
		$this->template->questionsAheadCount = $this->singleQuestionFacade->getQuestionsAheadCount($this->testProgress);
		$this->template->testProgress = $this->testProgress;
		$this->template->currentTestQuestion = $this->currentTestQuestion;
		$answersRevisions = $this->currentTestQuestion->getAnswers()->toArray();
		shuffle($answersRevisions);
		$this->template->currentTestQuestionsAnswersRevisions = $answersRevisions;
	}



	/**
	 * Single question. Answers rendered in template as HTML.
	 * 
	 * @return Form
	 */
	protected function createComponentQuestionForm()
	{
		$form = new Form;
		$form->addSubmit('send');
		$form->onSuccess[] = [$this, 'answer'];
		return $form;
	}



	/**
	 * Encodes answer's composite key into base64 encoded string.
	 * 
	 * @param Answer $answer
	 * @return string
	 */
	protected function encodeAnswer(Answer $answer)
	{
		return base64_encode(json_encode(['text' => $answer->getText(), 'isCorrect' => $answer->getIsCorrect()]));
	}



	/**
	 * Decodes answer's composite key as associative array from base64 encoded string.
	 * 
	 * @param string $string
	 * @return array
	 */
	protected function decodeAnswer($string)
	{
		return (array) json_decode(base64_decode($string));
	}



}

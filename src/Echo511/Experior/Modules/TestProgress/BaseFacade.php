<?php

namespace Echo511\Experior\Modules\TestProgress;

use Doctrine\ORM\NoResultException;
use Echo511\Experior\Domain\TestProgress;
use Kdyby\Doctrine\EntityManager;
use Nette\Object;

/**
 * @author Nikolas Tsiongas
 */
class BaseFacade extends Object
{

	/** @var EntityManager */
	private $em;

	function __construct(EntityManager $em)
	{
		$this->em = $em;
	}



	/**
	 * Get test.
	 * 
	 * @param int $id
	 */
	public function getTestProgress($id)
	{
		$result = $this->em->getDao(TestProgress::classname)
			->find($id);

		if ($result === null) {
			throw new NoResultException;
		}

		return $result;
	}



}

<?php

namespace Echo511\Experior\Modules\TestProgress;

use Nette\Application\UI\Form;

class NewPresenter extends BasePresenter
{

	/** @var NewFacade @inject */
	public $choiceFacade;

	public function renderDefault()
	{
		$this->template->tags = $this->choiceFacade->getTags();
	}



	public function actionNewFromTestProgress($id)
	{
		$testProgress = $this->choiceFacade->getTestProgress($id);
		if ($testProgress) {
			if ($testProgress->getSourceType() == 'questionsTags') {
				$settings = $testProgress->getSourceData();
				$numberOfQuestions = isset($settings['numberOfQuestions']) ? $settings['numberOfQuestions'] : 10;
				$tags = isset($settings['tags']) ? $settings['tags'] : [];

				$newTestProgress = $this->choiceFacade->startTest($numberOfQuestions, $tags, $this->user->getIdentity());
				$this->redirect(':Experior:TestProgress:SingleQuestion:default', ['id' => $newTestProgress->getId()]);
			}
		}
	}



	protected function createComponentChoiceForm()
	{
		$form = new Form;
		$form->addText('number')
			->addRule(Form::INTEGER, 'Počet otázek musí být číslo.');
		$form->addSubmit('run');
		$form->onSuccess[] = function (Form $form, $values ) {
			$testProgress = $this->choiceFacade->startTest($values->number, $form->getHttpData($form::DATA_TEXT, 'tags[]'), $this->user->getIdentity());
			$this->redirect(':Experior:TestProgress:SingleQuestion:default', ['id' => $testProgress->getId()]);
		};
		return $form;
	}



}

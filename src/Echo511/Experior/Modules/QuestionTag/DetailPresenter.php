<?php

namespace Echo511\Experior\Modules\QuestionTag;

use Echo511\Experior\Control\IEditableQuestionControlFactory;
use Echo511\Experior\Domain\QuestionTag;
use Echo511\Experior\Presenter\BasePresenter;
use Echo511\VisualPaginator\VisualPaginator;
use Kdyby\Doctrine\ResultSet;
use Nette\Application\UI\Control;
use Nette\Utils\Strings;

class DetailPresenter extends BasePresenter
{

	/** @var string @persistent */
	public $webalize;

	/** @var DetailFacade @inject */
	public $detailFacade;

	/** @var IEditableQuestionControlFactory @inject */
	public $editableQuestionControlFactory;

	/** @var QuestionTag */
	private $tag;

	/** @var ResultSet */
	private $questions;

	public function startup()
	{
		parent::startup();

		if (($title = $this->getParameter('title')) != null) {
			$tag = $this->detailFacade->getQuestionTagByTitle($title);
			if ($tag) {
				$this->redirect('this', [
				    'title' => null,
				    'webalize' => $tag->getWebalize()
				]);
			}
		}

		$this->tag = $this->detailFacade->getQuestionTag($this->webalize);

		if ($this->tag === null) {
			$this->flashMessage('Tag nebyl nalezen.');
			$this->redirect('List:default');
		}

		$this->questions = $this->detailFacade->getQuestions($this->tag);
	}



	public function handleStartTest()
	{
		$testProgress = $this->detailFacade->startTest($this->tag, new \Doctrine\Common\Collections\ArrayCollection((array) $this->questions->getIterator()));
		$this->redirect(':Experior:TestProgress:SingleQuestion:default', ['id' => $testProgress->getId()]);
	}



	public function renderDefault()
	{
		$paginator = $this['questionsPaginator']->getPaginator();
		$paginator->setItemsPerPage(10);
		
		
		$this->questions->applyPaginator($paginator);
		$this->template->tag = $this->tag;
		$this->template->questions = $this->questions;
	}



	/**
	 * Multiple questions support.
	 * 
	 * @param string $name
	 * @return Control
	 */
	public function createComponent($name)
	{
		if (Strings::startsWith($name, 'question_')) {
			list($component, $questionId) = explode('_', $name);
			return $this->editableQuestionControlFactory->create($this->detailFacade->getQuestion((int) $questionId));
		}
		return parent::createComponent($name);
	}



	/**
	 * @return VisualPaginator
	 */
	protected function createComponentQuestionsPaginator()
	{
		$paginator = new VisualPaginator();
		$paginator->changePageLink = function($page) {
			return $this->link('this', ['questionsPaginator-page' => $page]);
		};
		return $paginator;
	}



}

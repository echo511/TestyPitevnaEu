<?php

namespace Echo511\Experior\Modules\QuestionTag;

use Echo511\Experior\Presenter\BasePresenter;

class ListPresenter extends BasePresenter
{

	/** @var ListFacade @inject */
	public $listFacade;

	public function renderDefault()
	{
		$this->template->alphabetArray = $this->listFacade->getQuestionTagsInAlphabetArray();
	}



}

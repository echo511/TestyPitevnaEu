<?php

namespace Echo511\Experior\Modules\QuestionTag;

use Echo511\Experior\Domain\QuestionTag;
use Echo511\Experior\Query\QuestionTagDetailQuery;
use Kdyby\Doctrine\EntityManager;
use Nette\Object;
use Nette\Utils\Strings;

class ListFacade extends Object
{

	/** @var EntityManager */
	private $em;

	function __construct(EntityManager $em)
	{
		$this->em = $em;
	}



	/**
	 * Return [
	 * 	'A' => [Tag1, Tag2],
	 * 	'B' => [Tag3]
	 * ]
	 * 
	 * @return array
	 */
	public function getQuestionTagsInAlphabetArray()
	{
		$dao = $this->em->getDao(QuestionTag::classname);
		$tags = $dao->findAll();

		$alphabetArray = [];
		foreach ($tags as $tag) {
			$count = $dao->fetch(new QuestionTagDetailQuery($tag))
				->getTotalCount();
			if ($count > 0) {
				$alphabetArray[Strings::substring($tag->getTitle(), 0, 1)][] = [
				    'questionsCount' => $count,
				    'tag' => $tag
				];
			}
		}
		return $alphabetArray;
	}



}

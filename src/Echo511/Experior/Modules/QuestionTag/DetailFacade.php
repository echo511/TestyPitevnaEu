<?php

namespace Echo511\Experior\Modules\QuestionTag;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\NoResultException;
use Echo511\Experior\Criteria\QuestionCriteria;
use Echo511\Experior\Domain\Question;
use Echo511\Experior\Domain\QuestionTag;
use Echo511\Experior\Domain\TestProgressSource;
use Echo511\Experior\Manipulator\TestProgressManipulator;
use Echo511\Experior\Query\QuestionTagDetailQuery;
use Kdyby\Doctrine\EntityManager;
use Nette\Object;
use Nette\Security\User;

class DetailFacade extends Object
{

	/** @var EntityManager */
	private $em;

	/** @var TestProgressManipulator */
	private $testProgressManipulator;

	/** @var User */
	private $user;

	function __construct(EntityManager $em, TestProgressManipulator $testProgressManipulator, User $user)
	{
		$this->em = $em;
		$this->testProgressManipulator = $testProgressManipulator;
		$this->user = $user;
	}



	public function getQuestionTag($webalize)
	{
		return $this->em->getDao(QuestionTag::classname)
				->findOneBy(['webalize' => $webalize]);
	}



	public function getQuestionTagByTitle($title)
	{
		return $this->em->getDao(QuestionTag::classname)
				->find($title);
	}



	public function getQuestions(QuestionTag $tag)
	{
		return $this->em->getDao(Question::classname)
				->fetch(new QuestionTagDetailQuery($tag));
	}



	/**
	 * Get question from subset. Used to check if question belongs to tag.
	 * 
	 * @param Collection $questions
	 * @param int $id
	 * @return Question
	 * @throws NoResultException
	 */
	public function getQuestion($id)
	{
		return $this->em->getDao(Question::classname)
				->find($id);
	}



	public function startTest(QuestionTag $tag, Collection $questions)
	{
		$source = (new TestProgressSource)
			->setId($tag->getTitle())
			->setType('questionTag');
		$testProgress = $this->testProgressManipulator->createTestProgress($questions, $source)
			->setCreatedBy($this->user->getIdentity());
		$this->em->persist($testProgress);
		$this->em->flush();
		return $testProgress;
	}



}

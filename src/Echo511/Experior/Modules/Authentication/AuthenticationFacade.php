<?php

namespace Echo511\Experior\Modules\Authentication;

use Doctrine\ORM\NoResultException;
use Echo511\Experior\Domain\User;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Facebook\Facebook;
use Kdyby\Facebook\FacebookApiException;
use Nette\Object;
use Nette\Security\Identity;
use Nette\Security\User as NUser;
use Tracy\Debugger;

/**
 * Login via facebook, logout.
 * 
 * @author Nikolas Tsiongas
 */
class AuthenticationFacade extends Object
{

	/** @var EntityManager */
	private $em;

	/** @var Facebook */
	private $facebook;

	/** @var NUser */
	private $user;

	function __construct(EntityManager $em, Facebook $facebook, NUser $user)
	{
		$this->em = $em;
		$this->facebook = $facebook;
		$this->user = $user;
	}



	public function loginViaFacebook()
	{
		if (!$this->facebook->getUser()) {
			throw new AuthenticationException;
		}

		try {
			$dao = $this->em->getDao(User::classname);
			$me = $this->facebook->api('/me');

			try {
				$user = $dao->createQueryBuilder('u')
					->where('u.facebookId = :id', $this->facebook->getUser())
					->getQuery()
					->getSingleResult();
			} catch (NoResultException $e) {
				$user = (new User)
					->setFacebookId($me->id)
					->setAlias($me->name);

				if ($me->offsetExists('email')) {
					$user->setEmail($me->email);
				} else {
					$user->setEmail('noemail@experior.mediscipulus.eu');
				}
			}

			$user->setFacebookAccessToken($this->facebook->getAccessToken());

			$this->em->persist($user);
			$this->em->flush();

			$this->user->login(new Identity($user->getId(), null));
		} catch (FacebookApiException $e) {
			Debugger::log($e, 'facebook');
			throw new AuthenticationException;
		}
	}



	public function logout()
	{
		$this->user->logout(true);
	}



}

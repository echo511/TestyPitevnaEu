<?php

namespace Echo511\Experior\Modules\Authentication;

use Echo511\Experior\Presenter\BasePresenter;
use Kdyby\Facebook\Dialog\LoginDialog;
use Kdyby\Facebook\Facebook;

/**
 * Login & logout user.
 * 
 * @author Nikolas Tsiongas
 */
class AuthenticationPresenter extends BasePresenter
{

	/** @var AuthenticationFacade @inject */
	public $authenticationFacade;

	/** @var Facebook @inject */
	public $facebook;

	public function actionLogout()
	{
		$this->authenticationFacade->logout();
		$this->flashMessage('Byl(a) jste úspěšně odhlášen(a).', 'success');
		$this->redirect('this');
	}



	/**
	 * @return LoginDialog
	 */
	protected function createComponentFbLogin()
	{
		$dialog = $this->facebook->createDialog('login');

		$dialog->onResponse[] = function (LoginDialog $dialog) {
			try {
				$this->user->onLoggedIn[] = function() {
					$this->flashMessage('Byl(a) jste úspěšně přihlášen(a).', 'success');
					$this->redirect('this');
				};
				$this->authenticationFacade->loginViaFacebook();
			} catch (AuthenticationException $e) {
				$this->flashMessage("Authentizace selhala.");
			}
		};

		return $dialog;
	}



}

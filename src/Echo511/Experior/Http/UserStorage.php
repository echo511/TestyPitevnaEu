<?php

namespace Echo511\Experior\Http;

use Echo511\Experior\Domain\User;
use Kdyby\Doctrine\EntityManager;
use Nette\Http\Session;
use Nette\Http\UserStorage as NUserStorage;

/**
 * Return entity instead of identity.
 * 
 * @author Nikolas Tsiongas
 */
class UserStorage extends NUserStorage
{

	/** @var EntityManager */
	private $em;

	public function __construct(Session $sessionHandler, EntityManager $em)
	{
		parent::__construct($sessionHandler);
		$this->em = $em;
	}



	/**
	 * @return User
	 */
	public function getIdentity()
	{
		$identity = parent::getIdentity();
		if ($identity === null) {
			return null;
		}
		return $this->em->getDao(User::classname)->find($identity->getId());
	}



}

<?php

namespace Echo511\Gaufrette\Adapter;

interface Linkable
{

	function getUrl($key);
}

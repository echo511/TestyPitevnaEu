<?php

namespace Echo511\Gaufrette\Adapter;

use Gaufrette\Adapter\Local as GLocal;
use Gaufrette\Exception\FileNotFound;

class Local extends GLocal implements Linkable
{

	/** @var string */
	private $basePath;

	public function __construct($directory, $basePath = false, $create = false, $mode = 0777)
	{
		parent::__construct($directory, $create, $mode);
		$this->basePath = $basePath;
	}



	public function getUrl($key)
	{
		if (!$this->basePath) {
			throw new Exception('Cannot link to file. No base path provided.');
		}

		if ($this->exists($key)) {
			return $this->basePath . '/' . $key;
		}

		throw new FileNotFound($key);
	}



}

<?php

namespace Echo511\Gaufrette;

use Echo511\Gaufrette\Adapter\Linkable;
use Gaufrette\File as GFile;

class File extends GFile
{

	public function getUrl()
	{
		if ($this->filesystem->getAdapter() instanceof Linkable) {
			return $this->filesystem->getUrl($this->key);
		}
		return false;
	}



}

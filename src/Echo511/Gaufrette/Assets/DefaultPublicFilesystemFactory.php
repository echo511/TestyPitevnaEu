<?php

namespace Echo511\Gaufrette\Assets;

use Echo511\Gaufrette\Adapter\Local;

/**
 * Public filesystem as local directory accessible by web.
 * @author Nikolas Tsiongas
 */
class DefaultPublicFilesystemFactory implements IPublicFilesystemFactory
{

	const classname = __CLASS__;

	/** @var string */
	private $directory;

	/** @var string */
	private $basePath;

	public function __construct($directory, $basePath)
	{
		$this->directory = $directory;
		$this->basePath = $basePath;
	}



	public function create()
	{
		$adapter = new Local($this->directory, $this->basePath);
		return new PublicFilesystem($adapter);
	}



}

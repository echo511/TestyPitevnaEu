<?php

namespace Echo511\Gaufrette\Assets;

interface IPublicFilesystemFactory
{

	/** @return PublicFilesystem */
	function create();
}

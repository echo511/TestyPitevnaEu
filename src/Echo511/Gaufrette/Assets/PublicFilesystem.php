<?php

namespace Echo511\Gaufrette\Assets;

use Echo511\Gaufrette\Filesystem;

/**
 * Linkable filesystem for assets copies. Should not be accessed externally!
 * @author Nikolas Tsiongas
 */
class PublicFilesystem extends Filesystem
{

	const classname = __CLASS__;

}

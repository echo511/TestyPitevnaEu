<?php

namespace Echo511\Gaufrette\Assets;

use Latte\Macros\MacroSet;
use Nette\Bridges\ApplicationLatte\Template;
use Nette\Object;

class AssetMacro extends Object
{

	const classname = __CLASS__;

	/** @var ExtensionsAssets */
	private $extensionsData;

	public function __construct(ExtensionsAssets $extensionsData)
	{
		$this->extensionsData = $extensionsData;
	}



	public function register(Template $template, $name, $extensionClass)
	{
		$latte = $template->getLatte();

		$template->_echo511_assetlocator = $this->extensionsData;

		$set = new MacroSet($latte->getCompiler());
		$set->addMacro($name, function($node, $writer) use ($extensionClass) {
			return $writer->write('echo $_echo511_assetlocator->getFilesystem(\'' . $extensionClass . '\')->getUrl(\'' . trim($node->args, '"\'') . '\');');
		});
	}



}

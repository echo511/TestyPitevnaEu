<?php

namespace Echo511\Gaufrette;

use Gaufrette\Filesystem as GFilesystem;

class Filesystem extends GFilesystem
{

	public function getUrl($key)
	{
		if ($this->adapter instanceof Adapter\Linkable) {
			return $this->adapter->getUrl($key);
		}

		throw new \LogicException("Adapter must implement Linkable interface to provide URLs.");
	}



}

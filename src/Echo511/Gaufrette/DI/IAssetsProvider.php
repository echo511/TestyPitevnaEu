<?php

namespace Echo511\Gaufrette\DI;

interface IAssetsProvider
{

	/** @return string */
	function getDataDirectory();
}

<?php

namespace Echo511\Gaufrette\DI;

use Echo511\Gaufrette\Assets\AssetMacro;
use Echo511\Gaufrette\Assets\DefaultPublicFilesystemFactory;
use Echo511\Gaufrette\Assets\ExtensionsAssets;
use Echo511\Gaufrette\Assets\PublicFilesystem;
use Echo511\Gaufrette\Assets\WarmCommand;
use Kdyby\Console\DI\ConsoleExtension;
use Nette\DI\CompilerExtension;
use Nette\Http\RequestFactory;

class GaufretteExtension extends CompilerExtension
{

	private $config = array(
	    'publicDataDirectory' => '',
	    'publicDataBasePath' => '',
	    'publicFilesystemFactory' => 'Echo511\Gaufrette\Assets\DefaultPublicFilesystemFactory'
	);

	public function loadConfiguration()
	{
		$config = $this->getConfig($this->config);

		$this->containerBuilder->addDefinition($this->prefix('assetMacro'))
			->setClass(AssetMacro::classname);

		$this->containerBuilder->addDefinition($this->prefix('warmCommand'))
			->setClass(WarmCommand::classname)
			->addTag(ConsoleExtension::COMMAND_TAG);

		$this->containerBuilder->addDefinition($this->prefix('defaultPublicFilesystemFactory'))
			->setClass(DefaultPublicFilesystemFactory::classname)
			->setArguments(array(
			    $config['publicDataDirectory'],
			    $this->getBasePath() . '/' . $config['publicDataBasePath'],
		));

		$this->containerBuilder->addDefinition($this->prefix('publicFilesystem'))
			->setClass(PublicFilesystem::classname)
			->setFactory('@' . $config['publicFilesystemFactory'] . '::create');

		$this->prepareExtensionsFilesystems();
	}



	protected function prepareExtensionsFilesystems()
	{
		$mapping = array();
		foreach ($this->compiler->getExtensions('Echo511\Gaufrette\DI\IAssetsProvider') as $extension) {
			$mapping[get_class($extension)] = $extension->getDataDirectory();
		}
		$this->containerBuilder->addDefinition($this->prefix('extensionsData'))
			->setClass(ExtensionsAssets::classname)
			->setArguments(array($mapping));
	}



	protected function getBasePath()
	{
		$httpRequest = (new RequestFactory)->createHttpRequest();
		$baseUri = $baseUrl = rtrim($httpRequest->getUrl()->getBaseUrl(), '/');
		$basePath = preg_replace('#https?://[^/]+#A', '', $baseUrl);
		return $basePath;
	}



}

<?php

use Nette\Configurator;

require_once __DIR__ . '/../vendor/autoload.php';

$configurator = new Configurator();
$configurator->addParameters(array(
    'templateDir' => __DIR__ . '/../src/Echo511/Experior/templates',
    'publicDir' => __DIR__ . '/../public',
    'publicDirBasePath' => 'public',
));
$configurator->setTempDirectory(__DIR__ . '/../temp');
$configurator->createRobotLoader()
	->addDirectory(__DIR__ . '/../src')
	->addDirectory(__DIR__)
	->register();
$configurator->addConfig(__DIR__ . '/config.neon');
$configurator->addConfig(__DIR__ . '/config-local.neon');
return $configurator;

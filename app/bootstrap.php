<?php

$configurator = require_once __DIR__ . '/configurator.php';
$configurator->enableDebugger(__DIR__ . '/../log');
return $configurator->createContainer();

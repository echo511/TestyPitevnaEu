<?php

namespace App;

use Nette\Application\IRouter;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;

/**
 * Routing for experior.mediscipulus.eu
 * 
 * @author Nikolas Tsiongas
 */
final class RouterFactory
{

	/**
	 * 
	 * @return IRouter
	 */
	public function create()
	{
		$routeList = new RouteList();

		$this->createAuthenticationRoutes($routeList);

		$this->createQuestionsImportRoutes($routeList);

		$this->createTestProgressRoutes($routeList);

		$this->createQuestionTagRoutes($routeList);

		$this->createQuestionsChoosingRoutes($routeList);

		$routeList[] = new Route('<module>/<presenter>/<action>', array(
		    'module' => 'Experior:TestProgress',
		    'presenter' => 'List',
		    'action' => 'default'
		), Route::SECURED);

		return $routeList;
	}



	protected function createAuthenticationRoutes($routeList)
	{
		$routeList[] = new Route('login/', array(
		    'module' => 'Experior:Authentication',
		    'presenter' => 'Authentication',
		    'action' => 'login'
		), Route::SECURED);

		$routeList[] = new Route('logout/', array(
		    'module' => 'Experior:Authentication',
		    'presenter' => 'Authentication',
		    'action' => 'logout'
		), Route::SECURED);
	}



	protected function createQuestionsImportRoutes($routeList)
	{
		$routeList[] = new Route('importy/detail[/<id>[/strana/<questionsPaginator-page>]]/', array(
		    'module' => 'Experior:QuestionsImport',
		    'presenter' => 'Detail',
		    'action' => 'default'
		), Route::SECURED);

		$routeList[] = new Route('importy/novy/', array(
		    'module' => 'Experior:QuestionsImport',
		    'presenter' => 'Import',
		    'action' => 'default'
		), Route::SECURED);

		$routeList[] = new Route('importy/<presenter>/', array(
		    'module' => 'Experior:QuestionsImport',
		    'presenter' => 'List',
		    'action' => 'default'
		), Route::SECURED);
	}



	protected function createTestProgressRoutes($routeList)
	{
		$routeList[] = new Route('testy/novy/', array(
		    'module' => 'Experior:TestProgress',
		    'presenter' => 'New',
		    'action' => 'default'
		), Route::SECURED);

		$routeList[] = new Route('testy/test[/<id>]/', array(
		    'module' => 'Experior:TestProgress',
		    'presenter' => 'SingleQuestion',
		    'action' => 'default'
		), Route::SECURED);

		$routeList[] = new Route('testy/detail[/<id>]/', array(
		    'module' => 'Experior:TestProgress',
		    'presenter' => 'Detail',
		    'action' => 'default'
		), Route::SECURED);

		$routeList[] = new Route('testy/', array(
		    'module' => 'Experior:TestProgress',
		    'presenter' => 'List',
		    'action' => 'default'
		), Route::SECURED);
	}



	protected function createQuestionTagRoutes($routeList)
	{
		$routeList[] = new Route('tagy/', array(
		    'module' => 'Experior:QuestionTag',
		    'presenter' => 'List',
		    'action' => 'default'
		), Route::SECURED);

		$routeList[] = new Route('tagy/detail[/<webalize>[/strana/<questionsPaginator-page>]]/', array(
		    'module' => 'Experior:QuestionTag',
		    'presenter' => 'Detail',
		    'action' => 'default'
		), Route::SECURED);
	}



	protected function createQuestionsChoosingRoutes($routeList)
	{
		
	}



}

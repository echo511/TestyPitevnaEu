<?php

use Echo511\Experior\Domain\User;
use Echo511\Experior\Import\QuestionsImporter\JsonParser;
use Tester\Assert;

require_once __DIR__ . '/../bootstrap.php';

$questions = array(
    "Question1?" => array(
	"Answer1" => true,
	"Answer2" => true,
	"Answer3" => false,
    ),
    "Question2?" => array(
	"Answer1" => false,
	"Answer2" => true,
	"Answer3" => false,
    ),
    "Question3?" => array(
	"Answer1" => true,
	"Answer2" => false,
	"Answer3" => true,
    ),
);

$parser = new JsonParser();
$parsedQuestions = $parser->parse(json_encode($questions), new User);

reset($questions);
foreach ($parsedQuestions as $parseQuestion) {
	Assert::equal(key($questions), $parseQuestion->getText());
	$answers = current($questions);
	foreach ($parseQuestion->getAnswers() as $parsedAnswer) {
		Assert::equal(key($answers), $parsedAnswer->getText());
		Assert::equal(current($answers), $parsedAnswer->getIsCorrect());
		next($answers);
	}
	next($questions);
}

<?php

use Echo511\Experior\Domain\User;
use Echo511\Experior\Import\QuestionsImporter\TextParser;
use Tester\Assert;

require_once __DIR__ . '/../bootstrap.php';

$questions = array(
    "Question1?" => array(
	"Answer1" => true,
	"Answer2" => true,
	"Answer3" => false,
    ),
    "Question2?" => array(
	"Answer1" => false,
	"Answer2" => true,
	"Answer3" => false,
    ),
    "Question3?" => array(
	"Answer1" => true,
	"Answer2" => false,
	"Answer3" => true,
    ),
);

$text = 'Question1?
+Answer1
+Answer2
-Answer3
	
  
Question2?
-Answer1
+Answer2
-Answer3

Question3?
+Answer1
-Answer2
+Answer3';

$parser = new TextParser();
$parsedQuestions = $parser->parse($text, $user = new User);

reset($questions);
foreach ($parsedQuestions as $parseQuestion) {
	Assert::equal(key($questions), $parseQuestion->getText());
	Assert::equal($user, $parseQuestion->getCreatedBy());
	$answers = current($questions);
	foreach ($parseQuestion->getAnswers() as $parsedAnswer) {
		Assert::equal(key($answers), $parsedAnswer->getText());
		Assert::equal(current($answers), $parsedAnswer->getIsCorrect());
		next($answers);
	}
	next($questions);
}

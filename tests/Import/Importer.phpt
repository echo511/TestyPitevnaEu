<?php

use Echo511\Experior\Domain\User;
use Echo511\Experior\Import\QuestionsImporter\Importer;
use Echo511\Experior\Import\QuestionsImporter\JsonParser;
use Tester\Assert;

require_once __DIR__ . '/../bootstrap.php';

$questions = array(
    "Question1?" => array(
	"Answer1" => true,
	"Answer2" => true,
	"Answer3" => false,
    ),
    "Question2?" => array(
	"Answer1" => false,
	"Answer2" => true,
	"Answer3" => false,
    ),
    "Question3?" => array(
	"Answer1" => true,
	"Answer2" => false,
	"Answer3" => true,
    ),
);

$em = Mockery::mock('Kdyby\Doctrine\EntityManager');
$em->shouldReceive('persist')->times(1);
$em->shouldReceive('flush')->once();

$importer = new Importer($em);
$importer->import(json_encode($questions), new JsonParser(), new User);

try {
	$em->mockery_verify();
	Assert::equal(true, true);
} catch (Exception $e) {
	Assert::fail($e->getMessage());
}

<?php

use Tester\Environment;

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/helpers.php';

if (!class_exists('Tester\Assert')) {
	echo "Install Nette Tester using `composer update --dev`\n";
	exit(1);
}

if (!\Tracy\Debugger::isEnabled()) {
	Environment::setup();
}

$configurator = require(__DIR__ . '/../app/configurator.php');
DI::$container = $configurator->createContainer();

<?php

use Tracy\Debugger;

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/helpers.php';

if ($_SERVER['REMOTE_ADDR'] != '127.0.0.1' && $_SERVER['REMOTE_ADDR'] != 'localhost' && $_SERVER['REMOTE_ADDR'] != '::1') {
	die('Allowed only from localhost.');
}

if (!is_file(__DIR__ . '/' . $_GET['path'])) {
	die('Test not found.');
}

Debugger::$strictMode = true;
Debugger::enable(Debugger::DEVELOPMENT, __DIR__ . '/log');

require __DIR__ . '/' . $_GET['path'];

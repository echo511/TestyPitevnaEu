<?php

use Echo511\Experior\Domain\Answer;
use Echo511\Experior\Domain\Question;
use Echo511\Experior\Domain\QuestionTag;
use Echo511\Experior\Domain\User;
use Tester\Assert;

require_once __DIR__ . '/../bootstrap.php';

$answers = new \Doctrine\Common\Collections\ArrayCollection([
    $answer1 = new Answer,
    new Answer,
	]);
PropertyChanger::forceProperty($answer1, 'id', 123);
$tags = new \Doctrine\Common\Collections\ArrayCollection([
    new QuestionTag,
    new QuestionTag
	]);
$createdAt = new DateTime;
$createdBy = new User;

$question = new Question;

Assert::type('Doctrine\Common\Collections\ArrayCollection', $question->getAnswers());
Assert::type('DateTime', $question->getCreatedAt());
Assert::null($question->getCreatedBy());
Assert::type('Doctrine\Common\Collections\ArrayCollection', $question->getTags());
Assert::null($question->getText());
Assert::equal($question, $question->getFirstVersion());
Assert::equal($question, $question->getVersions()->first());

$question->setAnswers($answers)
	->setCreatedAt($createdAt)
	->setCreatedBy($createdBy)
	->setTags($tags)
	->setText('Text of question.');

Assert::equal($answers, $question->getAnswers());
Assert::equal($answer1, $question->getAnswer(123));
Assert::equal($createdAt, $question->getCreatedAt());
Assert::equal($createdBy, $question->getCreatedBy());
Assert::equal($tags, $question->getTags());
Assert::equal('Text of question.', $question->getText());

$nextVersion = $question->addVersion();
Assert::equal([$question, $nextVersion], $question->getVersions()->toArray());
Assert::equal($question->getVersions(), $nextVersion->getVersions());

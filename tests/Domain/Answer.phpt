<?php

use Echo511\Experior\Domain\Answer;
use Tester\Assert;

require_once __DIR__ . '/../bootstrap.php';

$answer = new Answer;

Assert::null($answer->getIsCorrect());
Assert::null($answer->getText());

$answer->setIsCorrect(true)
	->setText('text');

Assert::equal(true, $answer->getIsCorrect());
Assert::equal('text', $answer->getText());

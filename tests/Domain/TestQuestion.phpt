<?php

use Doctrine\Common\Collections\ArrayCollection;
use Echo511\Experior\Domain\Answer;
use Echo511\Experior\Domain\Question;
use Echo511\Experior\Domain\TestProgress;
use Echo511\Experior\Domain\TestQuestion;

require_once __DIR__ . '/../bootstrap.php';

$testQuestion = new TestQuestion;

DoctrineAssert::isCollection($testQuestion->getAnswered());
DoctrineAssert::null($testQuestion->getAnsweredAt());
DoctrineAssert::null($testQuestion->getId());
DoctrineAssert::null($testQuestion->getIsCorrectlyAnswered());
DoctrineAssert::null($testQuestion->getQuestion());
DoctrineAssert::null($testQuestion->getTestProgress());

$answeredAt = new DateTime;
$question = new Question;
$testProgress = new TestProgress;

$testQuestion->setAnsweredAt($answeredAt)
	->setQuestion($question)
	->setTestProgress($testProgress);

DoctrineAssert::equal($answeredAt, $testQuestion->getAnsweredAt());
DoctrineAssert::equal($question, $testQuestion->getQuestion());
DoctrineAssert::equal($testProgress, $testQuestion->getTestProgress());
DoctrineAssert::contains($testQuestion, $testProgress->getTestQuestions()->toArray());


// Switching test progress

$testProgress2 = new TestProgress;
$testProgress2->addTestQuestion($testQuestion);
DoctrineAssert::contains($testQuestion, $testProgress2->getTestQuestions()->toArray());
DoctrineAssert::notContains($testQuestion, $testProgress->getTestQuestions()->toArray());
DoctrineAssert::equal($testProgress2, $testQuestion->getTestProgress());

$testQuestion->setTestProgress($testProgress);
DoctrineAssert::contains($testQuestion, $testProgress->getTestQuestions()->toArray());
DoctrineAssert::notContains($testQuestion, $testProgress2->getTestQuestions()->toArray());
DoctrineAssert::equal($testProgress, $testQuestion->getTestProgress());


// Check if answered is correct

$answerTrue = (new Answer)->setIsCorrect(true);
PropertyChanger::forceProperty($answerTrue, 'id', 1);
$answerFalse = (new Answer)->setIsCorrect(false);
PropertyChanger::forceProperty($answerFalse, 'id', 2);

$question->setAnswers(new ArrayCollection([$answerTrue, $answerFalse]));
$testQuestion->answer([1]);
DoctrineAssert::true($testQuestion->getIsCorrectlyAnswered());

PropertyChanger::forceProperty($testQuestion, 'answered', new ArrayCollection);
$testQuestion->answer([2]);
DoctrineAssert::false($testQuestion->getIsCorrectlyAnswered());

PropertyChanger::forceProperty($testQuestion, 'answered', new ArrayCollection);
$testQuestion->answer([1, 2]);
DoctrineAssert::false($testQuestion->getIsCorrectlyAnswered());

DoctrineAssert::equal(false, $answeredAt === $testQuestion->getAnsweredAt()); // When answered change time (overwrite previous setAnswered())
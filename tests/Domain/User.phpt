<?php

use Echo511\Experior\Domain\User;
use Tester\Assert;

require_once __DIR__ . '/../bootstrap.php';

Assert::equal(true, true);die;

$createdAt = new DateTime;

$user = new User;

Assert::null($user->getAlias());
Assert::type('DateTime', $user->getCreatedAt());
Assert::null($user->getEmail());
Assert::null($user->getFacebookAccessToken());
Assert::null($user->getFacebookId());
Assert::null($user->getUsername());
Assert::equal(false, $user->isPasswordCorrect('wrongPass'));

$user->setAlias('John Doe')
	->setCreatedAt($createdAt)
	->setEmail('john.doe@gmail.com')
	->setFacebookAccessToken(123456)
	->setFacebookId(1)
	->setPassword('pass')
	->setUsername('jdoe');

Assert::equal('John Doe', $user->getAlias());
Assert::equal($createdAt, $user->getCreatedAt());
Assert::equal('john.doe@gmail.com', $user->getEmail());
Assert::equal(123456, $user->getFacebookAccessToken());
Assert::equal(1, $user->getFacebookId());
Assert::equal(true, $user->isPasswordCorrect('pass'));
Assert::equal(false, $user->isPasswordCorrect('wrongPass'));
Assert::equal('jdoe', $user->getUsername());

<?php

use Doctrine\Common\Collections\ArrayCollection;
use Echo511\Experior\Domain\Answer;
use Echo511\Experior\Domain\Question;
use Echo511\Experior\Domain\TestProgress;
use Echo511\Experior\Domain\TestQuestion;
use Echo511\Experior\Domain\User;

require_once __DIR__ . '/../bootstrap.php';

$progress = new TestProgress;

DoctrineAssert::isDate($progress->getCreatedAt());
DoctrineAssert::null($progress->getCreatedBy());
DoctrineAssert::null($progress->getId());
DoctrineAssert::false($progress->getIsComplete());
DoctrineAssert::noResult(function() use ($progress) {
	$progress->getRandomUnansweredTestQuestion();
});
DoctrineAssert::null($progress->getScore());
DoctrineAssert::null($progress->getSourceId());
DoctrineAssert::null($progress->getSourceType());
DoctrineAssert::noResult(function() use ($progress) {
	$progress->getTestQuestion(123);
});
DoctrineAssert::isCollection($progress->getTestQuestions());
DoctrineAssert::isCollection($progress->getUnansweredTestQuestions());

$createdAt = new DateTime;
$createdBy = new User;
$testQuestions = new ArrayCollection([
    $question1 = (new TestQuestion())->setQuestion((new Question)->setAnswers(new ArrayCollection([
	$answerTrue = (new Answer)->setIsCorrect(true),
	(new Answer)->setIsCorrect(false),
    ]))),
    $question2 = (new TestQuestion())->setQuestion((new Question)->setAnswers(new ArrayCollection([
	(new Answer)->setIsCorrect(true),
	$answerFalse = (new Answer)->setIsCorrect(false),
    ])))
	]);

PropertyChanger::forceProperty($question1, 'id', 125);
PropertyChanger::forceProperty($answerTrue, 'id', 1);
PropertyChanger::forceProperty($answerFalse, 'id', 1);

$progress->setCreatedAt($createdAt)
	->setCreatedBy($createdBy)
	->setSourceId(123)
	->setSourceType('source')
	->setTestQuestions($testQuestions);

DoctrineAssert::equal($createdAt, $progress->getCreatedAt());
DoctrineAssert::equal($createdBy, $progress->getCreatedBy());
DoctrineAssert::equal(123, $progress->getSourceId());
DoctrineAssert::equal('source', $progress->getSourceType());
DoctrineAssert::equal($testQuestions, $progress->getTestQuestions());
foreach ($progress->getTestQuestions() as $testQuestion) {
	DoctrineAssert::equal($progress, $testQuestion->getTestProgress());
}
DoctrineAssert::equal($question1, $progress->getTestQuestion(125));
DoctrineAssert::noResult(function() use($progress) {
	$progress->getTestQuestion(1);
});
DoctrineAssert::false($progress->getIsComplete());
DoctrineAssert::equal(0.0, $progress->getScore());

$question1->answer([1]);

DoctrineAssert::equal($question2, $progress->getRandomUnansweredTestQuestion());

$question2->answer([1]);

DoctrineAssert::noResult(function() use ($progress) {
	$progress->getRandomUnansweredTestQuestion();
});
DoctrineAssert::equal(0.5, $progress->getScore());
DoctrineAssert::equal(true, $progress->getIsComplete());


$progress->removeTestQuestion($question2);
DoctrineAssert::notContains($question2, $progress->getTestQuestions()->toArray());
DoctrineAssert::equal(1.0, $progress->getScore());

$progress->addTestQuestion($question2);
DoctrineAssert::contains($question2, $progress->getTestQuestions()->toArray());
DoctrineAssert::equal(0.5, $progress->getScore());

<?php

use Echo511\Experior\Domain\Question;
use Echo511\Experior\Domain\QuestionTag;
use Tester\Assert;

require_once __DIR__ . '/../bootstrap.php';

$question = new Question;

$tag = new QuestionTag;

Assert::null($tag->getId());
Assert::type('Doctrine\Common\Collections\ArrayCollection', $tag->getQuestions());
Assert::null($tag->getTitle());

$tag->setTitle('tag')
	->addQuestion($question);

Assert::equal('tag', $tag->getTitle());
Assert::equal($question, $tag->getQuestions()->first());
Assert::equal($tag, $question->getTags()->first());

$tag->removeQuestion($question);
Assert::equal(false, $tag->getQuestions()->first());
Assert::equal(false, $question->getTags()->first());

<?php

use Doctrine\Common\Collections\ArrayCollection;
use Echo511\Experior\Domain\Question;
use Echo511\Experior\Domain\QuestionsImport;
use Echo511\Experior\Domain\User;

require_once __DIR__ . '/../bootstrap.php';

$question = new Question;
PropertyChanger::forceProperty($question, 'id', 123);
$createdAt = new DateTime;
$createdBy = new User;
$questions = new ArrayCollection([
    new Question,
    new Question,
    $question
	]);

$import = new QuestionsImport;

DoctrineAssert::null($import->getId());
DoctrineAssert::isCollection($import->getQuestions());
DoctrineAssert::isDate($import->getCreatedAt());
DoctrineAssert::null($import->getCreatedBy());

$import->setCreatedAt($createdAt)
	->setCreatedBy($createdBy)
	->setQuestions($questions);

DoctrineAssert::equal($createdAt, $import->getCreatedAt());
DoctrineAssert::equal($createdBy, $import->getCreatedBy());
DoctrineAssert::equal($questions, $import->getQuestions());
DoctrineAssert::equal($question, $import->getQuestion(123));
DoctrineAssert::noResult(function() use ($import) {
	$import->getQuestion(2);
});

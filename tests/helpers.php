<?php

use Nette\Reflection\ClassType;
use Tester\Assert;

class DI
{

	public static $container;

}

class DoctrineAssert extends Assert
{

	public static function isDate($actual)
	{
		return self::type('DateTime', $actual);
	}



	public static function isCollection($actual)
	{
		return self::type('Doctrine\Common\Collections\Collection', $actual);
	}



	public static function noResult($callback)
	{
		return self::throws($callback, 'Doctrine\ORM\NoResultException');
	}



}

class PropertyChanger
{

	public static function forceProperty($object, $property, $value)
	{
		$reflection = new ClassType($object);
		$property = $reflection->getProperty($property);
		$property->setAccessible(true);
		$property->setValue($object, $value);
		$property->setAccessible(false);
	}



}
